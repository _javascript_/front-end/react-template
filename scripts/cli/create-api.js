const path = require("path");
const fs = require("fs");

const { API_PATH } = require("./paths");

const {
  getApiRepoTmpl,
  getApiRepoCrudTmpl,
  getApiDtoTmpl,
  getApiUpdateDtoTmpl,
  getApiDtoExportsTmpl,
  getApiEntityTmpl,
  getApiEntityExportsTmpl,
  getApiFacadeTmpl,
} = require("./templates");

const {
  upperCase,
  getFileName,
  skewerCase,
  createDirs,
  createFiles,
  pluralToSingular,
  camelize,
} = require("./utils");

const { EXTS, ENCODING, REGEXPS } = require("./consts");

async function main(name, api, isCrud, endpoint) {
  await addApiStructure(name, api, isCrud, endpoint);
  await updateApiFacade(name, api);
}

async function addApiStructure(name, api, isCrud, endpoint) {
  const SKEWER_CASE_NAME = skewerCase(name);
  const SINGULAR_NAME = pluralToSingular(name);

  const NEW_DIR_PATH = path.join(API_PATH, api, SKEWER_CASE_NAME);
  const DTO_PATH = path.join(NEW_DIR_PATH, "dto");
  const ENTITIES_PATH = path.join(NEW_DIR_PATH, "entities");
  const TYPES_PATH = path.join(NEW_DIR_PATH, "types");

  const paths = [NEW_DIR_PATH, DTO_PATH, ENTITIES_PATH, TYPES_PATH];

  createDirs(paths);

  const names = getApiFilesName(SINGULAR_NAME);
  const tmpls = getApiTemplates(SINGULAR_NAME, isCrud, endpoint);

  const keysDto = ["createDto", "updateDto", "entityDto", "dtoFacade"];
  const keysEntities = ["entitys", "entitysFacade"];
  const keysTypes = ["typesFacade"];
  const keysRoot = ["repository", "facade"];

  const files = Object.entries(names).reduce((acc, [key, name]) => {
    const item = { name, tmpl: tmpls[key], path: null };
    if (keysDto.some((k) => k === key)) item.path = DTO_PATH;
    if (keysEntities.some((k) => k === key)) item.path = ENTITIES_PATH;
    if (keysTypes.some((k) => k === key)) item.path = TYPES_PATH;
    if (keysRoot.some((k) => k === key)) item.path = NEW_DIR_PATH;
    acc.push(item);
    return acc;
  }, []);

  createFiles(files);
}

function getApiFilesName(name) {
  const upperCaseName = upperCase(name);

  function createNameDto(action = "") {
    return `${upperCase(action)}${upperCaseName}Dto`;
  }

  const createDto = getFileName(createNameDto("create"));
  const updateDto = getFileName(createNameDto("update"));
  const entityDto = getFileName(createNameDto());
  const dtoFacade = getFileName();

  const entitys = getFileName(upperCaseName, EXTS.TS);
  const entitysFacade = getFileName();

  const typesFacade = getFileName();

  const repository = getFileName("repository", EXTS.TS);
  const facade = getFileName();

  return {
    createDto,
    updateDto,
    entityDto,
    dtoFacade,
    entitys,
    entitysFacade,
    typesFacade,
    repository,
    facade,
  };
}

function getApiTemplates(name, isCrud, endpoint) {
  const upperCaseName = upperCase(name);

  const createDto = getApiDtoTmpl(`Create${upperCaseName}`);
  const updateDto = getApiUpdateDtoTmpl(upperCaseName);
  const entityDto = getApiDtoTmpl(`${upperCaseName}`);
  const dtoFacade = getApiDtoExportsTmpl(upperCaseName);

  const entitys = getApiEntityTmpl(upperCaseName);
  const entitysFacade = getApiEntityExportsTmpl(upperCaseName);

  const typesFacade = `export {};`;

  const repository = isCrud
    ? getApiRepoCrudTmpl(upperCaseName, endpoint)
    : getApiRepoTmpl(upperCaseName, endpoint);
  const facade = getApiFacadeTmpl();

  return {
    createDto,
    updateDto,
    entityDto,
    dtoFacade,
    entitys,
    entitysFacade,
    typesFacade,
    repository,
    facade,
  };
}

async function updateApiFacade(name, api) {
  const skewerCaseApi = skewerCase(api);
  const camelizeSaceApi = camelize(api);

  const skewerCaseName = skewerCase(name);
  const camelizeSaceName = camelize(name);

  const PATH_FACADE = path.join(API_PATH, getFileName());
  const PATH_WORK_FOLDER_API = path.join(API_PATH, skewerCaseApi);
  const PATH_WORK_FOLDER_FACADE = path.join(
    PATH_WORK_FOLDER_API,
    getFileName()
  );
  const PATH_WORK_FOLDER_FETCH = path.join(
    PATH_WORK_FOLDER_API,
    getFileName("fetch", EXTS.TS)
  );

  checkFetchFile(PATH_WORK_FOLDER_FETCH);
  updateApiWorkFolderFacade(
    PATH_WORK_FOLDER_FACADE,
    camelizeSaceName,
    skewerCaseName
  );
  updateApiRootFacade(PATH_FACADE, camelizeSaceApi, skewerCaseApi);
}

function updateApiRootFacade(path, name, api) {
  const facadeData = `export * as ${name} from "./${api}";`;

  if (fs.existsSync(path)) {
    const fileData = fs.readFileSync(path, ENCODING.UTF8);
    const lines = fileData.split("\n");
    if (
      !lines.some((line) => {
        const paths = line.match(REGEXPS.QUOTES);
        if (!paths) return false;
        return paths.some((p) => {
          return p === `./${api}`;
        });
      })
    ) {
      lines.push(facadeData);
      const newData = lines.join("\n");
      fs.writeFileSync(path, newData);
    }
  } else {
    fs.writeFileSync(path, facadeData);
  }
}

function updateApiWorkFolderFacade(path, camelizeSaceName, skewerCaseName) {
  const facadeData = `export * as ${camelizeSaceName} from "./${skewerCaseName}";`;

  if (fs.existsSync(path)) {
    const fileData = fs.readFileSync(path, ENCODING.UTF8);
    const lines = fileData.split("\n");
    if (
      !lines.some((line) => {
        const paths = line.match(REGEXPS.QUOTES);
        if (!paths) return false;
        return paths.some((p) => {
          return p === `./${skewerCaseName}`;
        });
      })
    ) {
      lines.push(facadeData);
      const newData = lines.join("\n");
      fs.writeFileSync(path, newData);
    }
  } else {
    const fetchData = `export { customFetch } from "./fetch";\n`;
    fs.writeFileSync(path, fetchData + facadeData);
  }
}

function checkFetchFile(path) {
  if (!fs.existsSync(path)) {
    const fetchData = `import { $fetch } from "shared/lib";
  
  export const customFetch = $fetch.create();`;
    fs.writeFileSync(path, fetchData);
  }
}

module.exports = {
  apiInit: main,
};
