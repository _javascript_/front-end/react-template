const { getFileName } = require("./utils");

const REGEXPS = {
  QUOTES: /"((?:\\.|[^"\\])*)"/,
};

const EXTS = { SVG: "svg", TSX: "tsx", TS: "ts", CSS: "css" };

const ENCODING = { UTF8: "utf8" };

const FILE_NAME = {
  PATHS: getFileName("paths", EXTS.TS),
  PAGES: getFileName("pages", EXTS.TS),
};

const CINFIG_CONST_NAMES = {
  PAGE_NAMES: "PAGE_NAMES",
  PATHS: "PATHS",
};

const MAPPING_TYPE_NUMBER = {
  entity: "1",
  feature: "2",
  page: "3",
  "ui-component": "4",
  api: "5",
};

const MAPPING_NUMBER_TYPE = Object.fromEntries(
  Object.entries(MAPPING_TYPE_NUMBER).map(([key, value]) => [value, key])
);

module.exports = {
  EXTS,
  FILE_NAME,
  ENCODING,
  CINFIG_CONST_NAMES,
  MAPPING_NUMBER_TYPE,
  MAPPING_TYPE_NUMBER,
  REGEXPS,
};
