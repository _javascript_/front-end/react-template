const fs = require("fs");
const path = require("path");

const { UI_PATH } = require("./paths");

const {
  getUIFacade,
  getUIComponent,
  getUIComponentCss,
  getUIComponentsFacade,
  getUIComponentContainer,
  getUIComponentContainerCss,
} = require("./templates");

const { createDirs, createFiles, getFileName, upperCase } = require("./utils");

const { EXTS, ENCODING, REGEXPS } = require("./consts");

async function main(name, path) {
  await addComponentStructure(name, path);
  await updateUIFacade(name, path);
}

async function addComponentStructure(name, dir = "atoms") {
  const SKEWER_CASE_NAME = upperCase(name);

  const FACADE_UI_PATH = path.join(UI_PATH, dir);
  const NEW_DIR_PATH = path.join(FACADE_UI_PATH, SKEWER_CASE_NAME);
  const UI_COMPONENTS_PATH = path.join(NEW_DIR_PATH, "components");

  const paths = [NEW_DIR_PATH, UI_COMPONENTS_PATH];
  createDirs(paths);

  const names = getUIFilesName(name);
  const tmpls = getUITemplates(name);

  const keysFacade = ["facade", "comp", "compCss"];
  const keysComponents = ["compsFacade", "compContainer", "compContainerCss"];

  const check = (key) => (k) => k === key;

  const files = Object.entries(names).reduce((acc, [key, name]) => {
    const item = { name, tmpl: tmpls[key], path: null };
    if (keysFacade.some(check(key))) item.path = NEW_DIR_PATH;
    if (keysComponents.some(check(key))) item.path = UI_COMPONENTS_PATH;
    acc.push(item);
    return acc;
  }, []);

  createFiles(files);
}

function getUITemplates(name) {
  const facade = getUIFacade(name);
  const comp = getUIComponent(name);
  const compCss = getUIComponentCss(name);
  const compsFacade = getUIComponentsFacade(name);
  const compContainer = getUIComponentContainer(name);
  const compContainerCss = getUIComponentContainerCss(name);

  return {
    facade,
    comp,
    compCss,
    compsFacade,
    compContainer,
    compContainerCss,
  };
}

function getUIFilesName(name) {
  const upperCaseName = upperCase(name);

  const facade = getFileName();
  const comp = getFileName(upperCaseName, EXTS.TSX);
  const compCss = getFileName(upperCaseName, EXTS.CSS);
  const compsFacade = getFileName();
  const compContainer = getFileName("Container", EXTS.TSX);
  const compContainerCss = getFileName("Container", EXTS.CSS);

  return {
    facade,
    comp,
    compCss,
    compsFacade,
    compContainer,
    compContainerCss,
  };
}

async function updateUIFacade(name, dir = "atoms") {
  const upperCaseName = upperCase(name);
  const FACADE_UI_PATH = path.join(UI_PATH, dir, getFileName());

  const facadeData = `export * from "./${upperCaseName}";`;

  if (fs.existsSync(FACADE_UI_PATH)) {
    const fileData = fs.readFileSync(FACADE_UI_PATH, ENCODING.UTF8);
    const lines = fileData.split("\n");
    if (
      !lines.some((line) => {
        const paths = line.match(REGEXPS.QUOTES);
        if (!paths) return false;
        return paths.some((p) => {
          return p === `./${upperCaseName}`;
        });
      })
    ) {
      lines.push(facadeData);
      const newData = lines.join("\n");
      fs.writeFileSync(FACADE_UI_PATH, newData);
    }
  } else {
    fs.writeFileSync(FACADE_UI_PATH, facadeData);
  }
}

module.exports = {
  componentInit: main,
};
