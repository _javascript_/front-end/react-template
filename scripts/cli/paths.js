const path = require("path");

const ROOT = process.cwd();
const SRC_PATH = path.join(ROOT, "src");

const ENTITIES_PATH = path.join(SRC_PATH, "entities");
const SHARED_PATH = path.join(SRC_PATH, "shared");
const PAGES_PATH = path.join(SRC_PATH, "pages");
const FEATURES_PATH = path.join(SRC_PATH, "features");

const API_PATH = path.join(SHARED_PATH, "api");
const UI_PATH = path.join(SHARED_PATH, "ui");
const CONFIG_PATH = path.join(SHARED_PATH, "config");

module.exports = {
  ROOT,
  SRC_PATH,
  ENTITIES_PATH,
  SHARED_PATH,
  PAGES_PATH,
  FEATURES_PATH,
  API_PATH,
  UI_PATH,
  CONFIG_PATH,
};
