const {
  constansCase,
  upperCase,
  camelize,
  skewerCase,
  pluralToSingular,
} = require("./utils");

const generatePageFacade = (name) => {
  const constansCaseName = constansCase(name);
  return `import React from "react";

    import { LAYOUT_NAMES } from "shared/config/layout";
    import { PAGE_NAMES } from "shared/config/pages";
    import { PATHS } from "shared/config/paths";
    import { PageConfig } from "shared/lib/router/page";

    const Page = React.lazy(() => import("./ui"));

    export const ${constansCaseName}: PageConfig = {
      pageName: PAGE_NAMES.${constansCaseName},
      layout: LAYOUT_NAMES.HEADER_FOOTER,
      path: PATHS[PAGE_NAMES.${constansCaseName}](),
      access: {
        allowed: undefined,
        redirect: undefined,
      },
      component: Page,
    };`;
};

const generatePage = (name) => {
  const upperCaseName = upperCase(name);

  return `import React, { useEffect } from "react";
    import { useStore } from "effector-react";
    import { clsx } from "shared/utils";

    import { i18n, router } from "shared/lib";
    import { Page, Container, Loader } from "shared/ui";

    import * as pageService from "../service";

    import { PageTitle } from "./components";

    export type ${upperCaseName}PageProps = router.InjectionProps & {};

    const ${upperCaseName}Page = (props: ${upperCaseName}PageProps) => {
      const helmet = i18n.useHelmetTranslation(props.metadata.pageName);
      const firstInit = useStore(pageService.stores.$firstInitialization);

      useEffect(() => {
        if (firstInit) return;
        pageService.actions.init();
      }, []);

      return (
        <Page helmet={helmet}>
          <Container variant="center" tag="div">
            <PageTitle />
          </Container>
        </Page>
      );
    };

    export default ${upperCaseName}Page;`;
};

const generateUiExports = (name) => {
  const upperCaseName = upperCase(name);
  return `export { default } from "./${upperCaseName}Page";`;
};

const generateUiComponentsExports = () => {
  return `export * from "./PageTitle";`;
};

const generateUiComponentPageTitle = (name) => {
  const upperCaseName = upperCase(name);
  return `import React from "react";
    export const PageTitle = () => <h1>${upperCaseName} Page</h1>;`;
};

const generateService = (name) => {
  return `import { createDomain } from "effector";

  const pageDomain = createDomain("${name}");

  const $firstInitialization = pageDomain.createStore<boolean>(false);
  const init = pageDomain.createEvent();
  $firstInitialization.on(init, () => true);

  export const stores = {
    $firstInitialization,
  };

  export const actions = {
    init,
  };`;
};

const getUIExportsTmpl = (name) => {
  return `export * from "./${upperCase(name)}";`;
};

const getEntityFacadeTmpl = (name) => {
  return `export * as ${camelize(name)}Service from "./service";
export * from "./ui"`;
};

const getUIComponentTmpl = () => {
  return `import React from "react";

export type ComponentProps = { value: string; }

export const Component = ({ value }: ComponentProps) => <p>Component: { value }</p>;
`;
};

const getUIEntityTmpl = (name) => {
  const upperCaseName = upperCase(name);
  const camelCaseName = camelize(name);
  return `import React from "react";
import { Component } from './components'

export type ${upperCaseName}Props = {
  ${camelCaseName}: any;
}

export const ${upperCaseName} = (props: ${upperCaseName}Props) => {
  const [value, setValue] = React.useState<string>("TEST");

  return (
    <div>
      <h3>${upperCaseName}</h3>
      <Component value={value} />
    </div>
  );
}`;
};

const getEntityServiceTmpl = (name) => {
  const singularName = pluralToSingular(name);

  const camelCaseName = camelize(name);
  const upperCaseName = upperCase(name);

  const camelCaseSingularName = camelize(singularName);
  const upperCaseSingularName = upperCase(singularName);

  return `import { createDomain, forward } from "effector"

type ${upperCaseSingularName} = {}
type ${upperCaseName} = ${upperCaseSingularName}[]

const domain = createDomain("${camelCaseName}");

const $${camelCaseSingularName} = domain.createStore<${upperCaseSingularName} | null>(null);
const $${camelCaseName} = domain.createStore<${upperCaseName}>([]);

const get${upperCaseSingularName}Fx = domain.createEffect(async () => {
  throw new Error('Реализуй метод')
});

const get${upperCaseName}Fx = domain.createEffect(async () => {
  throw new Error('Реализуй метод')
});

const reset${upperCaseSingularName} = domain.createEvent();
const reset${upperCaseName} = domain.createEvent();

$${camelCaseSingularName}.on(get${upperCaseSingularName}Fx.doneData, (_, ${camelCaseSingularName}) => ${camelCaseSingularName});
$${camelCaseSingularName}.reset(reset${upperCaseSingularName});

$${camelCaseName}.on(get${upperCaseName}Fx.doneData, (_, ${camelCaseName}) => ${camelCaseName});
$${camelCaseName}.reset(reset${upperCaseName});

export const stores = {
  $${camelCaseSingularName},
  $${camelCaseName},
};
export const actions = {
  get${upperCaseSingularName}Fx,
  get${upperCaseName}Fx,
  reset${upperCaseSingularName},
  reset${upperCaseName},
};`;
};

const getEntityCrudServiceTmpl = (name, api) => {
  const singularName = pluralToSingular(name);

  const camelCaseApi = camelize(api);

  const camelCaseName = camelize(name);
  const upperCaseName = upperCase(name);

  const camelCaseSingularName = camelize(singularName);
  const upperCaseSingularName = upperCase(singularName);

  const pathToApi = (name) => `${camelCaseApi}.${camelCaseName}.${name}`;
  const entityType = pathToApi(upperCaseSingularName);
  const createDto = pathToApi(`Create${upperCaseSingularName}Dto`);
  const updateDto = pathToApi(`Update${upperCaseSingularName}Dto`);
  const repo = pathToApi("repo");

  return `import { createDomain, forward } from "effector"

import { ${camelCaseApi} } from "shared/api";

type ${upperCaseSingularName} = ${entityType};
type ${upperCaseName} = ${upperCaseSingularName}[];
type Create${upperCaseSingularName}Dto = ${createDto};
type Update${upperCaseSingularName}Dto = ${updateDto};

const domain = createDomain("${camelCaseName}");

const select${upperCaseSingularName} = domain.createEvent<${upperCaseSingularName}>();
const select${upperCaseSingularName}ById = domain.createEvent<number>();

const get${upperCaseSingularName}Fx = domain.createEffect(async (id: number) => {
  return ${repo}.findOne(id);
});

const get${upperCaseName}Fx = domain.createEffect(async () => {
  return ${repo}.findAll();
});

const reset${upperCaseSingularName} = domain.createEvent();
const reset${upperCaseName} = domain.createEvent();

const update${upperCaseSingularName}Fx = domain.createEffect((id: number, dto: Update${upperCaseSingularName}Dto) => {
    return ${repo}.update(id, dto);
  }
);

const create${upperCaseSingularName}Fx = domain.createEffect((dto: Create${upperCaseSingularName}Dto) => {
  return ${repo}.create(dto);
});

const delete${upperCaseSingularName}Fx = domain.createEffect((id: number) => {
  return ${repo}.remove(id);
});

const $${camelCaseSingularName} = domain.createStore<${upperCaseSingularName} | null>(null);
$${camelCaseSingularName}.on(get${upperCaseSingularName}Fx.doneData, (_, ${camelCaseSingularName}) => ${camelCaseSingularName});
$${camelCaseSingularName}.on(select${upperCaseSingularName}, (_, ${camelCaseSingularName}) => ${camelCaseSingularName});
$${camelCaseSingularName}.reset(reset${upperCaseSingularName});

const $${camelCaseName} = domain.createStore<${upperCaseName}>([]);
$${camelCaseName}.on(get${upperCaseName}Fx.doneData, (_, ${camelCaseName}) => ${camelCaseName});
$${camelCaseName}.reset(reset${upperCaseName});

forward({
  from: [update${upperCaseSingularName}Fx, create${upperCaseSingularName}Fx, delete${upperCaseSingularName}Fx],
  to: get${upperCaseName}Fx,
});

forward({
  from: select${upperCaseSingularName}ById,
  to: get${upperCaseSingularName}Fx
})

export const stores = {
  $${camelCaseSingularName},
  $${camelCaseName},
};

export const actions = {
  select${upperCaseSingularName},
  select${upperCaseSingularName}ById,
  get${upperCaseSingularName}Fx,
  get${upperCaseName}Fx,
  update${upperCaseSingularName}Fx,
  create${upperCaseSingularName}Fx,
  delete${upperCaseSingularName}Fx,
  reset${upperCaseSingularName},
  reset${upperCaseName},
};`;
};

const getApiRepoTmpl = (name, endpoint) => {
  return `import { customFetch } from "..";
import { Repository } from "shared/lib";
import { ${name} } from "./entities/${name}";
import { Create${name}Dto, Update${name}Dto } from "./dto";

class ${name}Repository extends Repository<${name}> {
  constructor() {
    super(customFetch, "/${endpoint}", ${name});
  }
}

export const repo = new ${name}Repository();
`;
};

const getApiRepoCrudTmpl = (name, endpoint) => {
  return `import { customFetch } from "..";
import { Repository } from "shared/lib";
import { ${name} } from "./entities/${name}";
import { Create${name}Dto, Update${name}Dto } from "./dto";

class ${name}Repository extends Repository<${name}> {
  constructor() {
    super(customFetch, "/${endpoint}", ${name});
  }

  create(dto: Create${name}Dto) {
    return super.createEntity(dto);
  }

  findAll() {
    return super.findAllEntitys();
  }

  findOne(id: number) {
    return super.findOneEntity(id);
  }

  update(id: number, dto: Update${name}Dto) {
    return super.updateEntity(id, dto);
  }

  remove(id: number) {
    return super.removeEntity(id);
  }
}

export const repo = new ${name}Repository();
`;
};

const getApiDtoTmpl = (name) => {
  return `export type ${name}Dto = {};`;
};

const getApiUpdateDtoTmpl = (name) => {
  return `import { Create${name}Dto } from "./Create${name}Dto";

export type Update${name}Dto = Partial<Create${name}Dto> & {};
`;
};

const getApiDtoExportsTmpl = (name) => {
  return `export * from "./Create${name}Dto";
export * from "./Update${name}Dto";
export * from "./${name}Dto";
`;
};

const getApiEntityTmpl = (name) => {
  return `import { Entity } from "shared/lib/transport/repo";
import { ${name}Dto } from "../dto";

export class ${name} extends Entity implements ${name}Dto {
  constructor(dto: ${name}Dto) {
    super();
  }
}`;
};

const getApiEntityExportsTmpl = (name) => {
  return `export * from "./${name}";`;
};

const getApiFacadeTmpl = () => {
  return `export * from "./dto";
export * from "./entities";
export * from "./types";
export * from "./repository";
  `;
};

const getUIFacade = (name) => {
  const upperCaseName = upperCase(name);

  return `export * from "./${upperCaseName}";`;
};

const getUIComponent = (name) => {
  const upperCaseName = upperCase(name);
  const skewerCaseName = skewerCase(name);
  return `import React from "react";
import { clsx } from "shared/utils";

import { Container } from "./components";

import "./${upperCaseName}.css";

export type ${upperCaseName}Props = {};

export const ${upperCaseName} = (props: ${upperCaseName}Props) => {
  return (
    <div className={clsx("${skewerCaseName}")}>
      <h3>${upperCaseName}</h3>
      <Container>Container</Container>
    </div>
  );
};
`;
};

const getUIComponentCss = (name) => {
  return `.${skewerCase(name)} {
  }`;
};

const getUIComponentsFacade = (name) => {
  return `export * from "./Container";`;
};

const getUIComponentContainer = (name) => {
  return `import { clsx } from "shared/utils";
import React from "react";

import "./Container.css";

export type ContainerProps = {
  children: React.ReactNode;
};

export const Container = ({ children }: ContainerProps) => {
  return <div className={clsx("${skewerCase(
    name
  )}__container")}>{children}</div>;
};`;
};

const getUIComponentContainerCss = (name) => {
  return `.${skewerCase(name)}__container {
  }`;
};

module.exports = {
  generatePageFacade,
  generatePage,
  generateUiExports,
  generateUiComponentPageTitle,
  generateUiComponentsExports,
  generateService,
  getEntityServiceTmpl,
  getUIEntityTmpl,
  getUIExportsTmpl,
  getUIComponentTmpl,
  getEntityFacadeTmpl,
  getEntityCrudServiceTmpl,
  getApiRepoTmpl,
  getApiRepoCrudTmpl,
  getApiDtoTmpl,
  getApiUpdateDtoTmpl,
  getApiDtoExportsTmpl,
  getApiEntityTmpl,
  getApiEntityExportsTmpl,
  getApiFacadeTmpl,
  getUIFacade,
  getUIComponent,
  getUIComponentCss,
  getUIComponentsFacade,
  getUIComponentContainer,
  getUIComponentContainerCss,
};
