const { MAPPING_TYPE_NUMBER } = require("./consts");
const { Config } = require("./config");
const { pageInit } = require("./create-page");
const { entityInit } = require("./create-entity");
const { apiInit } = require("./create-api");
const { featureInit } = require("./create-feature");
const { componentInit } = require("./create-ui-component");

async function main() {
  const config = await Config.create();
  const { api, crud, name, path, type, endpoint } = config;
  console.log({ config });
  if (type === MAPPING_TYPE_NUMBER.entity) {
    await entityInit(name, api, crud, endpoint);
  } else if (type === MAPPING_TYPE_NUMBER.page) {
    await pageInit(name, path);
  } else if (type === MAPPING_TYPE_NUMBER.feature) {
    await featureInit(name, path);
  } else if (type === MAPPING_TYPE_NUMBER.api) {
    await apiInit(name, api, crud, endpoint);
  } else if (type === MAPPING_TYPE_NUMBER["ui-component"]) {
    await componentInit(name, path);
  }
}

main();
