const path = require("path");

const { FEATURES_PATH } = require("./paths");

const {
  getEntityFacadeTmpl,
  getEntityServiceTmpl,
  getUIComponentTmpl,
  getUIEntityTmpl,
  getUIExportsTmpl,
} = require("./templates");

const {
  upperCase,
  getFileName,
  skewerCase,
  createDirs,
  createFiles,
} = require("./utils");

const { EXTS } = require("./consts");

async function main(name) {
  await addFeatureStructure(name);
}

async function addFeatureStructure(name) {
  const SKEWER_CASE_NAME = skewerCase(name);

  const NEW_DIR_PATH = path.join(FEATURES_PATH, SKEWER_CASE_NAME);
  const UI_PATH = path.join(NEW_DIR_PATH, "ui");
  const SERVICE_PATH = path.join(NEW_DIR_PATH, "service");
  const UI_COMPONENTS_PATH = path.join(UI_PATH, "components");

  const paths = [NEW_DIR_PATH, UI_PATH, SERVICE_PATH, UI_COMPONENTS_PATH];
  createDirs(paths);

  const names = getFeatureFilesName(name);
  const tmpls = getFeatureTemplates(name);

  const files = Object.entries(names).reduce((acc, [key, name]) => {
    const item = { name, tmpl: tmpls[key], path: null };
    if (key === "facede") item.path = NEW_DIR_PATH;
    if (key === "service") item.path = SERVICE_PATH;
    if (key === "ui" || key === "uiFacade") item.path = UI_PATH;
    if (key === "component" || key === "components")
      item.path = UI_COMPONENTS_PATH;
    acc.push(item);
    return acc;
  }, []);

  createFiles(files);
}

function getFeatureTemplates(name) {
  const facede = getEntityFacadeTmpl(name);
  const service = getEntityServiceTmpl(name);
  const uiFacade = getUIExportsTmpl(name);
  const ui = getUIEntityTmpl(name);
  const components = getUIExportsTmpl("Component");
  const component = getUIComponentTmpl();

  return { facede, service, ui, uiFacade, components, component };
}

function getFeatureFilesName(name) {
  const facede = getFileName();
  const service = getFileName();
  const ui = getFileName(upperCase(name), EXTS.TSX);
  const uiFacade = getFileName();
  const components = getFileName();
  const component = getFileName("Component", EXTS.TSX);

  return { facede, service, ui, uiFacade, components, component };
}

module.exports = {
  featureInit: main,
};
