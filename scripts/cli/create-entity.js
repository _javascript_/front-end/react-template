const path = require("path");

const { ENTITIES_PATH } = require("./paths");

const {
  getEntityFacadeTmpl,
  getEntityServiceTmpl,
  getUIComponentTmpl,
  getUIEntityTmpl,
  getUIExportsTmpl,
  getEntityCrudServiceTmpl,
} = require("./templates");

const {
  upperCase,
  getFileName,
  skewerCase,
  createDirs,
  createFiles,
} = require("./utils");

const { EXTS } = require("./consts");

const { apiInit } = require("./create-api");

async function main(name, api, isCrud, endpoint) {
  await addEntityStructure(name, isCrud, api);
  if (api) await apiInit(name, api, isCrud, endpoint);
}

async function addEntityStructure(name, isCrud, api) {
  const SKEWER_CASE_NAME = skewerCase(name);

  const NEW_DIR_PATH = path.join(ENTITIES_PATH, SKEWER_CASE_NAME);
  const UI_PATH = path.join(NEW_DIR_PATH, "ui");
  const SERVICE_PATH = path.join(NEW_DIR_PATH, "service");
  const UI_COMPONENTS_PATH = path.join(UI_PATH, "components");

  const paths = [NEW_DIR_PATH, UI_PATH, SERVICE_PATH, UI_COMPONENTS_PATH];
  createDirs(paths);

  const names = getEntityFilesName(name);
  const tmpls = getEntityTemplates(name, isCrud, api);

  const files = Object.entries(names).reduce((acc, [key, name]) => {
    const item = { name, tmpl: tmpls[key], path: null };
    if (key === "facede") item.path = NEW_DIR_PATH;
    if (key === "service") item.path = SERVICE_PATH;
    if (key === "ui" || key === "uiFacade") item.path = UI_PATH;
    if (key === "component" || key === "components")
      item.path = UI_COMPONENTS_PATH;
    acc.push(item);
    return acc;
  }, []);

  createFiles(files);
}

function getEntityTemplates(name, isCrud, api) {
  const facede = getEntityFacadeTmpl(name);
  const service = isCrud
    ? getEntityCrudServiceTmpl(name, api)
    : getEntityServiceTmpl(name);
  const uiFacade = getUIExportsTmpl(name);
  const ui = getUIEntityTmpl(name);
  const components = getUIExportsTmpl("Component");
  const component = getUIComponentTmpl();

  return { facede, service, ui, uiFacade, components, component };
}

function getEntityFilesName(name) {
  const facede = getFileName();
  const service = getFileName();
  const ui = getFileName(upperCase(name), EXTS.TSX);
  const uiFacade = getFileName();
  const components = getFileName();
  const component = getFileName("Component", EXTS.TSX);

  return { facede, service, ui, uiFacade, components, component };
}

module.exports = {
  entityInit: main,
};
