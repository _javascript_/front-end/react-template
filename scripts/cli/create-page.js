const fs = require("fs");
const path = require("path");

const { PAGES_PATH, CONFIG_PATH } = require("./paths");

const {
  generatePageFacade,
  generateService,
  generatePage,
  generateUiComponentPageTitle,
  generateUiComponentsExports,
  generateUiExports,
} = require("./templates");

const {
  upperCase,
  snakeCase,
  constansCase,
  getFileName,
  skewerCase,
} = require("./utils");

const {
  FILE_NAME,
  ENCODING,
  CINFIG_CONST_NAMES,
  EXTS,
  REGEXPS,
} = require("./consts");

async function main(name, path) {
  await editConfigFiles(name, path);
  await createPageStructure(name);
  await updateDataRouteConfig(name);
}

async function editConfigFiles(name, pagePath) {
  const files = fs.readdirSync(CONFIG_PATH);
  const workingFileNames = [FILE_NAME.PAGES, FILE_NAME.PATHS];

  const constansCaseName = constansCase(name);
  const snakeCaseName = snakeCase(name);

  for (const file of files) {
    if (!workingFileNames.includes(file)) continue;
    const fileData = fs.readFileSync(
      path.join(CONFIG_PATH, file),
      ENCODING.UTF8
    );
    const lines = fileData.split("\n");

    const { data, endIndexLine, startIndexLine } = getDataFile(lines, file);

    if (
      data.some((line) => {
        const value = line.split(":")[1];
        if (!value) return false;
        const paths = value.trim().match(REGEXPS.QUOTES);
        if (!paths) return false;
        return paths.some((p) => p === pagePath);
      })
    ) {
      console.log(`Страница ${name} уже существует`);
      process.exit(0);
    }

    const index = data.length - 1;
    const item =
      file === FILE_NAME.PAGES
        ? `  ${constansCaseName}: "${snakeCaseName}",`
        : getItemPath(name, pagePath);

    data.splice(index, 0, item);

    lines.splice(startIndexLine, endIndexLine + 1, ...data);
    const result = lines.join("\n");

    fs.writeFileSync(path.join(CONFIG_PATH, file), result, (err) => {
      if (err) throw err;
    });
  }
}

async function createPageStructure(name) {
  const SKEWER_CASE_NAME = skewerCase(name);

  const PATH_NEW_DIR = path.join(PAGES_PATH, SKEWER_CASE_NAME);
  const PATH_UI = path.join(PATH_NEW_DIR, "ui");
  const PATH_UI_COMPONENTS = path.join(PATH_UI, "components");
  const PATH_SERVICE = path.join(PATH_NEW_DIR, "service");

  if (fs.readdirSync(PAGES_PATH).some((n) => n === SKEWER_CASE_NAME)) {
    return console.log("Папочки уже существует");
  }

  const upperCaseName = upperCase(name);

  fs.mkdirSync(PATH_NEW_DIR);
  fs.mkdirSync(PATH_UI);
  fs.mkdirSync(PATH_UI_COMPONENTS);
  fs.mkdirSync(PATH_SERVICE);

  const facade = generatePageFacade(name);
  const service = generateService(name);

  const ui = generateUiExports(name);
  const page = generatePage(name);
  const components = generateUiComponentsExports(name);
  const pageTitle = generateUiComponentPageTitle(name);

  fs.writeFileSync(path.join(PATH_NEW_DIR, getFileName()), facade);
  fs.writeFileSync(path.join(PATH_SERVICE, getFileName()), service);
  fs.writeFileSync(path.join(PATH_UI, getFileName()), ui);
  fs.writeFileSync(
    path.join(PATH_UI, getFileName(upperCaseName + "Page", EXTS.TSX)),
    page
  );
  fs.writeFileSync(path.join(PATH_UI_COMPONENTS, getFileName()), components);
  fs.writeFileSync(
    path.join(PATH_UI_COMPONENTS, getFileName("PageTitle", EXTS.TSX)),
    pageTitle
  );
}

async function updateDataRouteConfig(name) {
  const SKEWER_CASE_NAME = skewerCase(name);
  const NAME_CONSTANS_CASE = constansCase(name);

  const PATH_ROUTE_CONFIG = path.join(PAGES_PATH, getFileName("routes"));

  const START_KEY = "pageConfigs";
  const END_KEY = "];";
  const fileData = fs.readFileSync(PATH_ROUTE_CONFIG, ENCODING.UTF8);

  const lines = fileData.split("\n");
  const startIndex = lines.findIndex((l) => l.includes(START_KEY)) - 1;
  const startData = lines.slice(startIndex);
  const endIndex = startData.findIndex((l) => l.includes(END_KEY));
  const data = startData.slice(0, endIndex);

  const importLine = `import { ${NAME_CONSTANS_CASE} } from "./${SKEWER_CASE_NAME}";`;
  const configLine = `  ${NAME_CONSTANS_CASE},`;
  const result = [importLine, ...data, configLine];

  lines.splice(startIndex, endIndex, ...result);
  const total = lines.join("\n");

  fs.writeFileSync(PATH_ROUTE_CONFIG, total, (err) => {
    if (err) throw err;
  });
}

function getDataFile(lines, file) {
  const key =
    file === FILE_NAME.PAGES
      ? CINFIG_CONST_NAMES.PAGE_NAMES
      : CINFIG_CONST_NAMES.PATHS;

  const startIndexLine = lines.findIndex((line) => line.includes(key));
  const startData = lines.slice(startIndexLine);
  const endIndexLine = startData.findIndex((line) =>
    line.includes("} as const")
  );
  const data = startData.slice(0, endIndexLine + 1);
  return { data, startIndexLine, endIndexLine };
}

function getItemPath(name, pagePath) {
  const key = `[PAGE_NAMES.${constansCase(name)}]`;

  const [path, params] = pagePath.split(":");
  const args = !params ? "" : `${params} = ":${params}"`;
  const postfixPath = params ? `\${${params}}` : "";
  const value = `(${args}) => \`${path}${postfixPath}\``;
  return `  ${key}: ${value},`;
}

module.exports = { pageInit: main };
