const fs = require("fs");
const path = require("path");

function getFileName(name = "index", ext = "ts") {
  return `${name}.${ext}`;
}

function deleteFolder(path) {
  let files = [];
  if (fs.existsSync(path)) {
    files = fs.readdirSync(path);
    files.forEach((file) => {
      let curPath = path + "/" + file;
      if (fs.statSync(curPath).isDirectory()) {
        deleteFolder(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

function ucFirst(str) {
  return str[0].toUpperCase() + str.slice(1);
}

function ulFirst(str) {
  return str[0].toLowerCase() + str.slice(1);
}

function splitWords(string) {
  return string
    .replace(/\W+/g, " ")
    .split(/ |\B(?=[A-Z])/)
    .filter((word) => Boolean(word));
}

function camelize(string) {
  return splitWords(string)
    .map((word, index) => (index ? ucFirst(word) : ulFirst(word)))
    .join("");
}

function upperCase(string) {
  return splitWords(string)
    .map((word) => ucFirst(word))
    .join("");
}

const snakeCase = (string) => {
  return splitWords(string)
    .map((word) => word.toLowerCase())
    .join("_");
};

const skewerCase = (string) => {
  return splitWords(string)
    .map((word) => word.toLowerCase())
    .join("-");
};

const constansCase = (string) => {
  return splitWords(string)
    .map((word) => word.toUpperCase())
    .join("_");
};

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

async function question(query, type = "string") {
  return new Promise((resolve) => {
    readline.question(query, (answer) => {
      if (type === "boolean") {
        if (answer.toLowerCase() === "y") answer = true;
        else answer = false;
      }
      resolve(answer);
    });
  });
}

function createDirs(paths = []) {
  paths.forEach((path) => fs.mkdirSync(path, { recursive: true }));
}

function createFiles(files = []) {
  files.forEach(({ name, tmpl, path: p }) => {
    fs.writeFileSync(path.join(p, name), tmpl);
  });
}

function pluralToSingular(name = "") {
  if (name[name.length - 1] === "s") return name.slice(0, -1);
  return name;
}

module.exports = {
  constansCase,
  skewerCase,
  getFileName,
  deleteFolder,
  ucFirst,
  camelize,
  snakeCase,
  upperCase,
  readline,
  question,
  createDirs,
  createFiles,
  pluralToSingular,
};
