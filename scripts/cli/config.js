const { question, readline } = require("./utils");
const { MAPPING_NUMBER_TYPE } = require("./consts");

/*
  const options = { type: "", name: "", path: "", api: false, crud: false };
*/

class Config {
  static questions = {
    type: Object.entries(MAPPING_NUMBER_TYPE).reduce((acc, [number, name]) => {
      const msg = `${number}. ${name}\n`;
      return (acc += msg);
    }, "Что нужно создать?\n"),
    name: (type) => `Название ${MAPPING_NUMBER_TYPE[type]}?\n`,
    api: "Нужно добавить в api? (Y/n)\n",
    apiName: "Название api?\n",
    apiEntity: "Название сущности?\n",
    endpoint: "Url endpoint?\n",
    crud: "Добавить CRUD? (Y/n)\n",
    path: "Какой путь?\n",
    ui: "1. Atoms\n2. Molecules\n",
  };

  constructor(options) {
    this.type = options.type;
    this.name = options.name;
    this.path = options.path;
    this.api = options.api;
    this.crud = options.crud;
    this.endpoint = options.endpoint;
  }

  static async create() {
    let type, name, api, crud, path, endpoint;
    type = await question(Config.questions.type);
    if (!MAPPING_NUMBER_TYPE[type]) {
      console.log("Нужно ввести цифирку");
      process.exit(0);
    }
    name = await question(Config.questions.name(type));
    if (type === "1") {
      api = await question(Config.questions.api, "boolean");
      if (api) {
        api = await question(Config.questions.apiName);
        endpoint = await question(Config.questions.endpoint);
        crud = await question(Config.questions.crud, "boolean");
      }
    }
    if (type === "3") {
      path = await question(Config.questions.path);
    }
    if (type === "4") {
      const variant = await question(Config.questions.ui);
      if (variant === "1") path = "atoms";
      if (variant === "2") path = "molecules";
    }
    if (type === "5") {
      api = name;
      name = await question(Config.questions.apiEntity);
      endpoint = await question(Config.questions.endpoint);
      crud = await question(Config.questions.crud, "boolean");
    }
    readline.close();

    return new Config({ type, name, api, crud, path, endpoint });
  }
}

module.exports = { Config };
