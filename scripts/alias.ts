import fs from "fs";
import path from "path";

import { SRC_PATH } from "./paths";

const dirs = fs.readdirSync(SRC_PATH).filter((dir) => {
  return dir.split(".")[1] ? false : true;
});

export const alias = dirs.reduce((acc, dir) => {
  acc[dir] = path.join(SRC_PATH, dir);
  return acc;
}, {});
