import path from "path";

export const ROOT = process.cwd();
export const SRC_PATH = path.join(ROOT, "src");
export const CERT_PATHS = {
  KEY: path.join(ROOT, "cert", "key.pem"),
  CERT: path.join(ROOT, "cert", "cert.pem"),
};
export const WASM_PATHS = {
  MY_CRATE_1: path.join(ROOT, "wasm", "my-crate-1"),
  MY_CRATE_2: path.join(ROOT, "wasm", "my-crate-2"),
};
