"use strict";

const fs = require("fs");
const http = require("http");
const https = require("https");
const path = require("path");

const WebSocket = require("ws");
const { generateToken } = require("./utils");

const hostname = "127.0.0.1";
const port = 5000;

const isHttps = true;
const protocol = isHttps ? https : http;
const uri = `${isHttps ? "https" : "http"}://${hostname}:${port}`;

const options = {
  key: fs.readFileSync("../cert/key.pem"),
  cert: fs.readFileSync("../cert/cert.pem"),
};

const server = protocol.createServer(options, (req, res) => {
  const url = new URL(`${"https"}://${req.headers.host}${req.url}`);
  if (url.pathname === "/ws") {
    console.log("WEB_SOCKET");
  }
  res.writeHead(200);
  res.end();
});

server.listen(port, hostname, () => {
  console.log(`[Server]: started -> ${uri}`);
});

const ws = new WebSocket.Server({ server });

const users = {};
const emit = (userId, event, data) => {
  const receiver = users[userId];
  if (receiver) {
    const msg = JSON.stringify({ type: event, ...data });
    receiver.send(msg);
  }
};

ws.on("connection", (connection, req) => {
  let id;
  const ip = req.socket.remoteAddress;
  console.log(`[WebSocket]: Connected ${ip}`);

  connection.on("message", (data) => {
    const packet = JSON.parse(data);
    switch (packet.type) {
      case "init": {
        console.log("[WebSocket/Server]: TYPE -> init", packet);
        id = generateToken(5);

        users[id] = connection;

        connection.send(
          JSON.stringify({
            type: "init",
            id,
          })
        );
        break;
      }
      case "request":
        console.log("[WebSocket/Server]: TYPE -> request", packet);
        emit(packet.to, "request", { from: id });
        break;
      case "call":
        console.log("[WebSocket/Server]: TYPE -> call", packet);
        emit(packet.to, "call", { ...packet, from: id });
        break;
      case "end":
        console.log("[WebSocket/Server]: TYPE -> end", packet);
        emit(packet.to, "end", { from: id });
        break;
      case "disconnect":
        console.log("[WebSocket/Server]: TYPE -> disconnect", packet);
        emit(packet.to, "disconnect", { from: id });
        break;
      case "stream":
        console.log("[WebSocket/Server]: TYPE -> stream", packet);
        // emit(packet.to, "disconnect", { from: id });
        break;
    }
  });
  connection.on("close", () => {
    console.log(`Disconnected ${ip}`);
  });
});
