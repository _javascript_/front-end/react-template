const fs = require("fs");
const path = require("path");

const ROOT = process.cwd();
const SVG_PATH = path.join(ROOT, "scripts", "svg");
const PATH_RESULT = path.join(__dirname, "result");

const EXTS = { SVG: "svg", TSX: "tsx" };

const ENCODING = { UTF8: "utf8" };

// Читаем папочку
fs.readdir(SVG_PATH, (err, files) => {
  // Удаляем папочку
  deleteFolder(PATH_RESULT);
  // Создаём папочку
  fs.mkdir(PATH_RESULT, { recursive: true }, (err) => {
    if (err) throw err;
  });
  //   Итерируемся по всем файлам
  for (let i = 0; i < files.length; i++) {
    //   Название элемента
    const file = files[i];
    // Название файла + расширение
    const [fileName, ext] = file.split(".");
    // Если не свг то продолжаем цикл
    if (ext !== EXTS.SVG) continue;
    // Переводим название из snake_case в CamelCase
    const fileNameCamelCase = stringSnakeCaseToCamalCase(fileName);
    // Читаем файл и получаем содержимое
    const data = fs.readFileSync(path.join(SVG_PATH, file), ENCODING.UTF8);
    // Создаём шаблон
    const result = template(fileNameCamelCase, data);
    // Записываем файл
    fs.writeFileSync(
      path.join(PATH_RESULT, `${fileNameCamelCase}.${EXTS.TSX}`),
      result,
      (err) => {
        if (err) throw err;
      }
    );
  }
});

function deleteFolder(path) {
  let files = [];
  if (fs.existsSync(path)) {
    files = fs.readdirSync(path);
    files.forEach((file) => {
      let curPath = path + "/" + file;
      if (fs.statSync(curPath).isDirectory()) {
        deleteFolder(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

function template(name, data) {
  return `import React from 'react';

  export const ${name} = (props: React.SVGProps<SVGSVGElement>) => {
      return (
          ${data})
  };`;
}

function stringSnakeCaseToCamalCase(string) {
  return string
    .split("_")
    .map((str) => {
      const newStr = str[0].toUpperCase() + str.slice(1);
      return newStr;
    })
    .join("");
}

// const PROJECT_PATH_SVG = ["src", "shared", "assets", "svg"];
