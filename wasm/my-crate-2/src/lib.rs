use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
    #[wasm_bindgen(js_namespace = console)]
    fn warn(s: &str);
}

macro_rules! console_warn {
    ($($t:tt)*) => (warn(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen]
pub fn my_crate_1(name: &str) {
    console_warn!("{}", format!("[MY_CRATE_1]: Hello, {}!", name));
}
