import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";
import wasmPack from "vite-plugin-wasm-pack";

const fs = require("fs");
const { alias } = require("./scripts/alias");
const { CERT_PATHS, WASM_PATHS } = require("./scripts/paths");

const cert = {
  key: fs.readFileSync(CERT_PATHS.KEY),
  cert: fs.readFileSync(CERT_PATHS.CERT),
};

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  const protocol = process.env.VITE_APP_PROTOCOL;
  const https = protocol === "https" ? cert : false;

  return {
    plugins: [
      wasmPack(WASM_PATHS.MY_CRATE_1),
      wasmPack(WASM_PATHS.MY_CRATE_2),
      react(),
    ],
    resolve: { alias },
    server: {
      https,
      port: Number(process.env.VITE_APP_PORT) ?? 3000,
      host: process.env.VITE_APP_HOSTNAME ?? "127.0.0.1",
    },
  };
});
