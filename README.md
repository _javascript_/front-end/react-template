# 🚀 React template 🚀

Это шаблон для быстрого старта проекта.

# 🛠 Технологии:

- **ReactJS 18**
- **TypeScript**
- **Effector** (Стейт менеджер)
- **i18next** (Мультиязычность)
- **Vite** (Сборщик проекта)
- **Nodejs** (Для создания CLI)
- **Docker** (Для автоматизации процессов сборки)
- **WebAssembly** (Rust)

### Подробнее о технологиях

- **[TypeScript](https://www.typescriptlang.org/)** — Типизированный JS.

- **[Effector](https://effector.dev/)** — Легковестный стейтменеджер, с атомарными стейтами. (Хорошая альтернатива Redux)

- **[React Router v6](https://reactrouter.com/docs/en/v6/getting-started/overview)** — Роутинг в нашем реакт приложении.

- **Axios (Нет)** — Реализовал что-то похожее. Страшненькое, но работает.

- **[React Hooks](https://ru.reactjs.org/docs/hooks-intro.html)** — useState, useEffect, useRef, useCallback.

- **[BEM](https://ru.bem.info/methodology/quick-start/)** — Сss методология. БЭМ (Блок, Элемент, Модификатор) — компонентный подход к веб-разработке. В его основе лежит принцип разделения интерфейса на независимые блоки. Он позволяет легко и быстро разрабатывать интерфейсы любой сложности и повторно использовать существующий код, избегая «Copy-Paste».

- **[i18next](https://react.i18next.com/)** — Позволяет реализовать многоязычность.

- **[WebAssembly (Rust)](https://rustwasm.github.io/docs/wasm-pack/)** — Веб асемблер, для написания производитльного(если получиться) кода, средой выполнения будет браузер. Можно использовать высокоуровневые языки, такие как Си, C++, C#, Rust, Go.

- **[Docker](https://www.docker.com/)** — программное обеспечение для автоматизации развёртывания.

- **[Vite](https://vitejs.dev/)** — Сборщик проектов, хорошая альтернатива вебпаку.

- **[Nodejs](https://nodejs.org/dist/latest-v16.x/docs/api/)** — Использовал для написания простеньких скриптов.
  1. Альясы для Vite, скрипт при старте читает попку src и на основе содержимого создаётся конфиг.
  2. Написал CLI, при запуске скрипта позволяет выбрать элемент, и в зависимости от варианты создаёт стандартную файловую структору выбранного элемента.

# 👨‍👩‍👦‍👦 Зависимости

```json
"dependencies": {
    "compose-function": "^3.0.3",
    "effector": "^22.3.0",
    "effector-react": "^22.0.6",
    "i18next": "^21.6.16",
    "jwt-decode": "^3.1.2",
    "react": "^18.0.0",
    "react-dom": "^18.0.0",
    "react-helmet-async": "^1.3.0",
    "react-i18next": "^11.16.6",
    "react-router": "^6.3.0",
    "react-router-dom": "^6.3.0",
    "vite-plugin-wasm-pack": "^0.1.10"
},
"devDependencies": {
    "@babel/core": "^7.17.9",
    "@types/compose-function": "^0.0.30",
    "@types/node": "^17.0.24",
    "@types/react": "^18.0.0",
    "@types/react-dom": "^18.0.0",
    "@vitejs/plugin-react": "^1.3.0",
    "babel-loader": "^8.2.5",
    "typescript": "^4.6.3",
    "vite": "^2.9.2"
},
```

# 🏍 Comands

**Установка**

```bash
git clone https://gitlab.com/_javascript_/front-end/react-template.git
cd react-template
npm i
```

**Запуск**

```bash
npm run dev
```

**Сборка**

```bash
npm run build
```

**Сгенерировать cert**

```bash
npm run cert
```

**Скомпилить wasm**

```bash
npm run wasm
```

# 📈 [Архитектура -> feature-sliced](https://feature-sliced.design/)

Попытка реализовать что-то похожее 👆
