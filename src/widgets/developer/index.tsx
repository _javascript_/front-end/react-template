import React from "react";

import { DeveloperContent, DeveloperWidget } from "features/developer";

export type DeveloperProps = {};

export const Developer = (props: DeveloperProps) => {
  return (
    <DeveloperWidget>
      <DeveloperContent />
    </DeveloperWidget>
  );
};
