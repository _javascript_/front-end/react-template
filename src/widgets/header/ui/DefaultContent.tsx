import React, { useContext } from "react";
import { useTranslation } from "react-i18next";
import { clsx } from "shared/utils";

import { Button } from "shared/ui";
import { PATHS } from "shared/config/paths";

import { NavigationUI } from "features/navigation";
import { appContextConfig } from "features/app-context";
import { SignOutButton } from "features/auth";

import { Profile } from "entities/profile";

type DefaultContentProps = {};

export const DefaultContent = (props: DefaultContentProps) => {
  const { t } = useTranslation("pages");

  const { actions, account } = useContext(appContextConfig.ApplicationContext);
  const { email, profile } = account;
  const { firstName, lastName, patronymic } = profile as any;

  const btn = <SignOutButton signOut={() => actions.auth.signOut()} />;
  const fio = getFio(firstName, lastName, patronymic);
  const links = Object.values(PATHS)
    .filter((_, index) => index >= 2)
    .map((fn) => fn("404"));

  return (
    <>
      <NavigationUI.Links />
      <Profile className={clsx("header__profile")} email={email} fio={fio}>
        {links.map((link) => {
          return (
            <Button key={link} to={link} fullWidth>
              {t(link.split("/")[0])}
            </Button>
          );
        })}
        {btn}
      </Profile>
    </>
  );
};

const getFio = (fn = "Ананим", ln = "Ананимович", p = "Ананимумосович") => {
  return `${fn[0]}. ${ln[0]}. ${p}`;
};
