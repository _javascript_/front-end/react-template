import React from "react";

import { navigationUtils } from "features/navigation";

import { AdminContent } from "./AdminContent";
import { AuthContent } from "./AuthContent";

import { DefaultContent } from "./DefaultContent";

type ContentProps = {};

export const Content = (props: ContentProps) => {
  const currentPage = navigationUtils.useGetCurrentPage();

  let content = <DefaultContent />;

  if (navigationUtils.isAdmin(currentPage)) content = <AdminContent />;
  if (navigationUtils.isAuth(currentPage)) content = <AuthContent />;

  return <>{content}</>;
};
