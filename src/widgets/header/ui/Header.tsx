import React, { useContext } from "react";

import { Container } from "shared/ui";
import { Logotype } from "features/app-logotype";

import { Content } from "./Content";

import "./Header.css";

export type HeaderProps = {};

export const Header = (props: HeaderProps) => {
  return (
    <header className="header">
      <Container variant="center" className="header__container">
        <Logotype />
        <Content />
      </Container>
    </header>
  );
};
