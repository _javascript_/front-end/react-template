import React from "react";
import { useNavigate } from "react-router";
import { Button } from "shared/ui";

type AuthContentProps = {};

export const AuthContent = (props: AuthContentProps) => {
  const navigate = useNavigate();
  return (
    <div className="ml-auto">
      <Button onClick={() => navigate(-1)}>Go back</Button>
    </div>
  );
};
