import React from "react";

import { NavigationUI } from "features/navigation";

import "./Leftbar.css";

export type LeftbarProps = {};

export const Leftbar = (props: LeftbarProps) => {
  return (
    <aside className="leftbar">
      Leftbar
      <NavigationUI.Links variant="vertically" />
    </aside>
  );
};
