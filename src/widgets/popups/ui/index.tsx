import { PopupsUI } from "features/popups";
import { mappedPopups } from "../config";

export const Popups = () => {
  return <PopupsUI.GetParameterPopups components={mappedPopups} />;
};
