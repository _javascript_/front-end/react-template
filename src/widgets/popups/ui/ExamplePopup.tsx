import React from "react";

type Props = {
  isOpened: boolean;
  goBack: () => void;
};

export const ExamplePopup = ({ isOpened, goBack }: Props) => {
  return (
    <div style={{ padding: 16 }}>
      <h2>Popup2 TEST</h2>
      <button color="secondary" onClick={goBack}>
        Back
      </button>
    </div>
  );
};
