import { popupsTypes } from "features/popups";
import { ExamplePopup } from "../ui/ExamplePopup";

export const GET_ENUMS = {
  popup: {
    example: "example",
    colorPicker: "color-picker",
  },
};

export const mappedPopups: popupsTypes.MappedPopups = {
  [GET_ENUMS.popup.example]: ExamplePopup,
};
