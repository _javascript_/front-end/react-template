import React from "react";
import { Container } from "shared/ui";

import "./Footer.css";

type FooterProps = {};

export const Footer = (props: FooterProps) => {
  return (
    <footer className="footer">
      <Container className="footer__container" variant="center">
        Footer
      </Container>
    </footer>
  );
};
