import React, { Suspense } from "react";
import { Loader } from "shared/ui";

export const withSuspense = (component: () => React.ReactNode) => () =>
  <Suspense fallback={<Loader />}>{component()}</Suspense>;
