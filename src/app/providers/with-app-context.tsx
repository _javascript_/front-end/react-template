import React from "react";
import { appContextConfig, appContextService } from "features/app-context";

export const withApplicationContext =
  (component: () => React.ReactNode) => () => {
    const context = appContextService.useCreateApplicationContext();
    return (
      <appContextConfig.ApplicationContext.Provider value={context}>
        {component()}
      </appContextConfig.ApplicationContext.Provider>
    );
  };
