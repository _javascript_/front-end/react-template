import compose from "compose-function";

import { withRouter } from "./with-router";
import { withHelmet } from "./with-helmet";
import { withApplicationContext } from "./with-app-context";
import { withSuspense } from "./with-suspens";

import "./with-wasm";
import "shared/lib/i18n";
import "shared/assets/styles/index.css";

export const withProviders = compose(withRouter, withHelmet, withSuspense);
export const withCustomProviders = compose(withApplicationContext);
