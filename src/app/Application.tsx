import React from "react";

import { useInit } from "processes/init";

import { Routing } from "pages";

import { withProviders, withCustomProviders } from "./providers";

const App = () => {
  return <InitedProviders />;
};

const InitedProviders = withCustomProviders(() => {
  return (
    <>
      <Init />
      <Routing />
    </>
  );
});

const Init = () => {
  useInit();
  return null;
};

export default withProviders(App);
