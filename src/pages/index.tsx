import React, { useContext } from "react";
import { Navigate, Routes, Route } from "react-router";

import { PATH_ERROR_404 } from "shared/config/paths";
import { Loader } from "shared/ui";

import { Popups } from "widgets/popups";

import { navigationUtils } from "features/navigation";
import { appContextUtils, appContextConfig } from "features/app-context";

import { ROUTE_CONFIGS } from "./routes";
import { Layout } from "./layout";

type RoutingProps = {};

export const Routing: React.FC<RoutingProps> = () => {
  const context = useContext(appContextConfig.ApplicationContext);

  if (appContextUtils.isNotAppReady(context)) {
    return <Loader />;
  }

  return (
    <>
      <Pages />
      <Popups />
    </>
  );
};

const Pages = () => {
  const context = useContext(appContextConfig.ApplicationContext);

  const routes = navigationUtils.useGuardRoutes(ROUTE_CONFIGS, context.roles);
  const layout = navigationUtils.useCurrentLayoutVariant(ROUTE_CONFIGS);

  return (
    <Routes>
      <Route path="/" element={<Layout variant={layout} />}>
        {routes.map(({ element, index, path }) => {
          return (
            <Route key={path} path={path} index={index} element={element} />
          );
        })}
        <Route path="*" element={<Navigate to={PATH_ERROR_404} replace />} />
      </Route>
    </Routes>
  );
};
