import React from "react";
export const ErrorCode = ({ code }: { code: string }) => <h1>{code}</h1>;
