import React from "react";
import { useNavigate, useParams } from "react-router";

import { i18n, router, error } from "shared/lib";
import { Page, Container, Button } from "shared/ui";

import { navigationUtils } from "features/navigation";

import { ErrorCode } from "./components";

import "./Error.css";

export type ErrorPageProps = router.InjectionProps & {};

const { STATUS_MESSAGE } = error;

const ErrorPage = (props: ErrorPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);
  const navigate = useNavigate();
  const appNavigate = navigationUtils.useAppNavigation();
  const { code } = useParams();

  return (
    <Page helmet={helmet}>
      <Container className="error-page" variant="center" tag="section">
        {STATUS_MESSAGE[Number(code)] ?? STATUS_MESSAGE[404]}
        <ErrorCode code={code ?? ""} />
        <div className="error-page__action">
          <Button onClick={() => navigate(-1)}>Go back</Button>
          <Button onClick={appNavigate.unprotected}>Go home</Button>
        </div>
      </Container>
    </Page>
  );
};

export default ErrorPage;
