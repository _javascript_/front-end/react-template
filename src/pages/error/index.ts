import React from "react";

import { LAYOUT_NAMES } from "shared/config/layout";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";
import { PageConfig } from "shared/lib/router/page";

const Page = React.lazy(() => import("./ui"));

export const ERROR: PageConfig = {
  pageName: PAGE_NAMES.ERROR,
  layout: LAYOUT_NAMES.ONLY_CONTENT,
  path: PATHS[PAGE_NAMES.ERROR](),
  component: Page,
};
