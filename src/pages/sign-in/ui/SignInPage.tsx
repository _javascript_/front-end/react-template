import React from "react";

import { i18n, router } from "shared/lib";
import {
  Page,
  Container,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Button,
  Input,
  Backdrop,
} from "shared/ui";

import { navigationUtils } from "features/navigation";

import { useSignInForm } from "../service";

import "./SignIn.css";

export type SignInPageProps = router.InjectionProps & {};

const SignInPage = (props: SignInPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);
  const { inputs, isLoading, submit } = useSignInForm();

  const navigate = navigationUtils.useAppNavigation();

  return (
    <Page helmet={helmet}>
      {isLoading && <Backdrop />}
      <Container tag="section" className="sign-in">
        <Card className="sign-in__card">
          <CardHeader>
            <h3 className="sign-in__title">Sign In</h3>
          </CardHeader>
          <CardBody>
            {inputs.map((input) => {
              return <Input key={input.label} {...input} />;
            })}
          </CardBody>
          <CardFooter>
            <Button onClick={submit} className="sign-in__button">
              Sign In
            </Button>
            <Button onClick={navigate.sign_up}>Sign up</Button>
          </CardFooter>
        </Card>
      </Container>
    </Page>
  );
};

export default SignInPage;
