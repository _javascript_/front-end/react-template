import React, { useState } from "react";
import { useNavigate } from "react-router";

import { Button, TextField } from "shared/ui";
import { services } from "shared/api";

type Props = {};

export const SignInPage = (props: Props) => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const navigate = useNavigate();

  async function submit() {
    const result = await services.authorization.registration({
      email,
      password,
    });
    console.log({ result });
  }

  async function signIn() {
    const result = await services.authorization.signIn({ email, password });
    console.log({ result });
    navigate(-1);
  }

  function onChange(type: "email" | "password") {
    const setter = type === "email" ? setEmail : setPassword;
    return ({
      target: { value },
    }: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setter(value);
    };
  }

  return (
    <div>
      SignInPage <br />
      <TextField placeholder="email" onChange={onChange("email")} />
      <TextField placeholder="password" onChange={onChange("password")} />
      <br />
      <Button onClick={submit}>Register</Button>
      <br />
      <Button onClick={signIn}>Sign-in</Button>
    </div>
  );
};
