import React from "react";

import { common } from "shared/api";

import { LAYOUT_NAMES } from "shared/config/layout";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";
import { PageConfig } from "shared/lib/router/page";

const { RoleEnum } = common;

const Page = React.lazy(() => import("./ui"));

export const SIGN_IN: PageConfig = {
  pageName: PAGE_NAMES.SIGN_IN,
  layout: LAYOUT_NAMES.HEADER,
  path: PATHS[PAGE_NAMES.SIGN_IN](),
  access: {
    allowed: [RoleEnum.GUEST],
    redirect: {
      [RoleEnum.USER]: PATHS[PAGE_NAMES.PROTECTED](),
      [RoleEnum.ADMIN]: PATHS[PAGE_NAMES.ADMIN](),
    },
  },
  component: Page,
};
