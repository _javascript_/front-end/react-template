import React from "react";

import { LAYOUT_NAMES } from "shared/config/layout";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";
import { PageConfig } from "shared/lib/router/page";

const Page = React.lazy(() => import("./ui"));

export const UNPROTECTED: PageConfig = {
  pageName: PAGE_NAMES.UNPROTECTED,
  layout: LAYOUT_NAMES.HEADER_FOOTER,
  path: PATHS[PAGE_NAMES.UNPROTECTED](),
  access: {
    allowed: undefined,
    redirect: undefined,
  },
  component: Page,
};
