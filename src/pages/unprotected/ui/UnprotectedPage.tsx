import { clsx } from "shared/utils";
import React from "react";

import { i18n, router } from "shared/lib";
import { Page, Container } from "shared/ui";

import { Lorem, PageTitle } from "./components";

export type UnprotectedPageProps = router.InjectionProps & {};

const UnprotectedPage = (props: UnprotectedPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);

  return (
    <Page helmet={helmet}>
      <Container variant="center" tag="div">
        <PageTitle />
        <Lorem />
      </Container>
      <Container tag="section">
        <Container variant="center" tag="div">
          <h3>Firest Section</h3>
          <Lorem />
          <Lorem />
          <Lorem />
          <Lorem />
        </Container>
      </Container>
      <Container tag="section">
        <Container variant="center" tag="div">
          <h3>Second Section</h3>
          <Lorem />
          <Lorem />
          <Lorem />
        </Container>
      </Container>
      <Container tag="section">
        <Container variant="center" tag="div">
          <h3>Third Section</h3>
          <Lorem />
          <Lorem />
          <Lorem />
          <Lorem />
        </Container>
      </Container>
    </Page>
  );
};

export default UnprotectedPage;
