import React from "react";

import { common } from "shared/api";

import { LAYOUT_NAMES } from "shared/config/layout";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";
import { PageConfig } from "shared/lib/router/page";

const { RoleEnum } = common;

const Page = React.lazy(() => import("./ui"));

export const PROTECTED: PageConfig = {
  pageName: PAGE_NAMES.PROTECTED,
  layout: LAYOUT_NAMES.HEADER_FOOTER,
  path: PATHS[PAGE_NAMES.PROTECTED](),
  access: {
    allowed: [RoleEnum.USER, RoleEnum.ADMIN],
    redirect: {
      [RoleEnum.GUEST]: PATHS[PAGE_NAMES.SIGN_IN](),
    },
  },
  component: Page,
};
