import { clsx } from "shared/utils";
import React from "react";

import { i18n, router } from "shared/lib";
import { Page, Container } from "shared/ui";

import { PageTitle, Lorem } from "./components";

import "./ProtectedPage.css";

export type ProtectedPageProps = router.InjectionProps & {};

const ProtectedPage = (props: ProtectedPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);

  return (
    <Page helmet={helmet}>
      <Container variant="center" tag="div">
        <PageTitle />
        <Lorem />
      </Container>
      <Container
        tag="section"
        className={clsx("protected-page__first-section")}
      >
        <Container variant="center" tag="div">
          <h3>Firest Section</h3>
          <Lorem />
          <Lorem />
          <Lorem />
          <Lorem />
        </Container>
      </Container>
      <Container
        tag="section"
        className={clsx("protected-page__second-section")}
      >
        <Container variant="center" tag="div">
          <h3>Second Section</h3>
          <Lorem />
          <Lorem />
          <Lorem />
        </Container>
      </Container>
      <Container
        tag="section"
        className={clsx("protected-page__third-section")}
      >
        <Container variant="center" tag="div">
          <h3>Third Section</h3>
          <Lorem />
          <Lorem />
          <Lorem />
          <Lorem />
        </Container>
      </Container>
    </Page>
  );
};

export default ProtectedPage;
