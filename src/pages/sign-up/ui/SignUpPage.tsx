import React from "react";

import { i18n, router } from "shared/lib";
import {
  Page,
  Container,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Button,
  Input,
  Backdrop,
} from "shared/ui";

import { useSignUpForm } from "../service";

import "./SignUp.css";

export type SignUpPageProps = router.InjectionProps & {};

const SignUpPage = (props: SignUpPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);
  const { inputs, isLoading, submit } = useSignUpForm();

  return (
    <Page helmet={helmet}>
      {isLoading && <Backdrop />}
      <Container tag="section" className="sign-up">
        <Card className="sign-up__card">
          <CardHeader>
            <h3 className="sign-up__title">Sign Up</h3>
          </CardHeader>
          <CardBody>
            {inputs.map((input) => {
              return <Input key={input.label} {...input} />;
            })}
          </CardBody>
          <CardFooter>
            <Button onClick={submit} className="sign-up__button">
              Sign Up
            </Button>
          </CardFooter>
        </Card>
      </Container>
    </Page>
  );
};

export default SignUpPage;
