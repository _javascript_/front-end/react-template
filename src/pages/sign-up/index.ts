import React from "react";

import { common } from "shared/api";

import { LAYOUT_NAMES } from "shared/config/layout";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";
import { PageConfig } from "shared/lib/router/page";

const { RoleEnum } = common;

const Page = React.lazy(() => import("./ui"));

export const SIGN_UP: PageConfig = {
  pageName: PAGE_NAMES.SIGN_UP,
  layout: LAYOUT_NAMES.HEADER,
  path: PATHS[PAGE_NAMES.SIGN_UP](),
  access: {
    allowed: [RoleEnum.GUEST],
    redirect: PATHS[PAGE_NAMES.ERROR]("403"),
  },
  component: Page,
};
