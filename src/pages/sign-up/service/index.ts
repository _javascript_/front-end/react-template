import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";

import { appContextConfig } from "features/app-context";

type Event = React.ChangeEvent<HTMLInputElement>;

export const useSignUpForm = () => {
  const { t } = useTranslation("form");

  const ctx = useContext(appContextConfig.ApplicationContext);

  const [email, setEmail] = useState<string>("");
  const [password, setPasswprd] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);

  function reset() {
    setEmail("");
    setPasswprd("");
    setIsLoading(false);
  }

  const inputs = [
    {
      label: t("email.label"),
      placeholder: t("email.placeholder"),
      value: email,
      onChange: ({ target: { value } }: Event) => setEmail(value),
    },
    {
      label: t("password.label"),
      placeholder: t("password.placeholder"),
      value: password,
      onChange: ({ target: { value } }: Event) => setPasswprd(value),
    },
  ];

  function submit() {
    setIsLoading(true);
    ctx.actions.auth
      .signUp({ email, password })
      .then(() => {
        console.log("SIGN UP");
      })
      .finally(() => {
        setIsLoading(false);
        reset();
      });
  }

  return { submit, isLoading, inputs, reset };
};
