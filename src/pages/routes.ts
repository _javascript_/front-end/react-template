import { getRouteConfigs } from "shared/lib/router";

import { PROTECTED } from "./protected";
import { UNPROTECTED } from "./unprotected";
import { ERROR } from "./error";
import { SIGN_IN } from "./sign-in";
import { SIGN_UP } from "./sign-up";
import { ADMIN } from "./admin";
import { JSONPLACEHOLDER } from "./jsonplaceholder";

const pageConfigs = [
  PROTECTED,
  UNPROTECTED,
  ERROR,
  SIGN_IN,
  SIGN_UP,
  ADMIN,
  JSONPLACEHOLDER,
];

export const ROUTE_CONFIGS = getRouteConfigs(pageConfigs);
