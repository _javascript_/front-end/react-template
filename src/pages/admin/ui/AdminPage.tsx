import React from "react";

import { i18n, router } from "shared/lib";
import { Page, Container } from "shared/ui";

import { Title } from "./components";

export type AdminPageProps = router.InjectionProps & {};

const AdminPage = (props: AdminPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);

  return (
    <Page helmet={helmet}>
      <Container variant="center" tag="section">
        AdminPage
        <Title />
      </Container>
    </Page>
  );
};

export default AdminPage;
