import React from "react";

import { common } from "shared/api";

import { LAYOUT_NAMES } from "shared/config/layout";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";
import { PageConfig } from "shared/lib/router/page";

const { RoleEnum } = common;

const Page = React.lazy(() => import("./ui"));

export const ADMIN: PageConfig = {
  pageName: PAGE_NAMES.ADMIN,
  layout: LAYOUT_NAMES.HEADER_LEFTBAR,
  path: PATHS[PAGE_NAMES.ADMIN](),
  access: {
    allowed: [RoleEnum.ADMIN],
    redirect: {
      [RoleEnum.USER]: PATHS[PAGE_NAMES.SIGN_IN](),
      [RoleEnum.GUEST]: PATHS[PAGE_NAMES.UNPROTECTED](),
    },
  },
  component: Page,
};
