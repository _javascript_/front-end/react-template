import { createDomain } from "effector";

const pageDomain = createDomain("jsonplaceholder");

const $firstInitialization = pageDomain.createStore<boolean>(false);
const init = pageDomain.createEvent();
$firstInitialization.on(init, () => true);

export const stores = {
  $firstInitialization,
};

export const actions = {
  init,
};
