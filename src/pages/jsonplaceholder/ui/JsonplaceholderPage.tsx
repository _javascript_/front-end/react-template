import React, { useEffect } from "react";
import { useStore } from "effector-react";
import { clsx } from "shared/utils";

import { i18n, router } from "shared/lib";
import { Page, Container, Loader } from "shared/ui";

import { usersService, Users } from "entities/json/users";

import * as pageService from "../service";

import { PageTitle } from "./components";

export type JsonplaceholderPageProps = router.InjectionProps & {};

const JsonplaceholderPage = (props: JsonplaceholderPageProps) => {
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);
  const firstInit = useStore(pageService.stores.$firstInitialization);
  const users = useStore(usersService.stores.$users);
  const isLoading = useStore(usersService.actions.getUsersFx.pending);

  useEffect(() => {
    if (firstInit) return;
    pageService.actions.init();
  }, []);

  useEffect(() => {
    usersService.actions.getUsersFx();
    return () => usersService.actions.resetUsers();
  }, []);

  const action = (type: "select" | "delete") => (id: number) => {
    console.log(type, id);
    usersService.actions.deleteUserFx(id);
  };

  return (
    <Page helmet={helmet}>
      <Container variant="center" tag="div">
        <PageTitle />
      </Container>
      {isLoading ? (
        <Loader />
      ) : (
        <Users
          users={users}
          select={action("select")}
          delete={action("delete")}
        />
      )}
    </Page>
  );
};

export default JsonplaceholderPage;
