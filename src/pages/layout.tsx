import React from "react";

import { Developer } from "widgets/developer";
import { Footer } from "widgets/footer";
import { Header } from "widgets/header";
import { Leftbar } from "widgets/leftbar";
import { Breadcrumbs } from "widgets/breadcrumbs";

import { LayoutVariant, Layout as LayoutUI } from "shared/ui";

export const Layout = ({ variant }: { variant: LayoutVariant }) => {
  return (
    <LayoutUI
      variant={variant}
      breadcrumbs={<Breadcrumbs />}
      developer={<Developer />}
      footer={<Footer />}
      header={<Header />}
      leftbar={<Leftbar />}
    />
  );
};
