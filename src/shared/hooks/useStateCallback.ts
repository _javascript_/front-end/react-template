import React, { useRef, useEffect, useState } from "react";

export function useStateCallback<S>(initialState: S) {
  const [state, setState] = useState<S>(initialState);
  const myCallbacksList = useRef<any>([]);
  const setStateWithCallback = (newState: S, callback?: any) => {
    setState(newState);
    if (callback) myCallbacksList.current.push(callback);
  };
  useEffect(() => {
    myCallbacksList.current.forEach((callback: any) => callback());
    myCallbacksList.current = [];
  }, [state]);

  return [state, setStateWithCallback] as const;
}
