import { PAGE_NAMES } from "shared/config/pages";

export const PATHS = {
  [PAGE_NAMES.UNPROTECTED]: () => "/",
  [PAGE_NAMES.PROTECTED]: () => "protected",
  [PAGE_NAMES.SIGN_IN]: () => "sign_in",
  [PAGE_NAMES.SIGN_UP]: () => "sign_up",
  [PAGE_NAMES.ADMIN]: () => "admin",
  [PAGE_NAMES.ERROR]: (code = ":code") => `error/${code}`,
  [PAGE_NAMES.JSONPLACEHOLDER]: () => "jsonplaceholder",
} as const;

export const PATH_ERROR_403 = PATHS[PAGE_NAMES.ERROR]("403");
export const PATH_ERROR_404 = PATHS[PAGE_NAMES.ERROR]("404");
export const PATH_HOME = PATHS[PAGE_NAMES.UNPROTECTED]();
export const PATH_JSONPLACEHOLDER = PATHS[PAGE_NAMES.JSONPLACEHOLDER]();

const PATH_PAGE_NAMES = Object.fromEntries(
  Object.entries(PATHS).map(([name, fn]) => [fn().split("/")[0], name])
);

export const getPageNameByPath = (pathname: string): string => {
  const path = pathname.split("/")[1];
  return PATH_PAGE_NAMES[path ?? "/"];
};
