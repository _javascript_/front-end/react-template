export const PAGE_NAMES = {
  PROTECTED: "protected",
  UNPROTECTED: "unprotected",
  ADMIN: "admin",
  SIGN_IN: "sign_in",
  SIGN_UP: "sign_up",
  ERROR: "error",
  JSONPLACEHOLDER: "jsonplaceholder",
} as const;
