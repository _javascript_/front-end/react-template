import { getEnvVar } from "./_base";

export const NODE_ENV = getEnvVar("MODE");

export const IS_DEV_ENV = NODE_ENV === "development";
export const IS_PROD_ENV = NODE_ENV === "production";

export const JSONPLACEHOLDER_API_URI = getEnvVar(
  "VITE_APP_JSONPLACEHOLDER_API_URI"
);

export const SERVER_API_URI = getEnvVar("VITE_APP_SERVER_API_URI");
export const SERVER_WS_URI = getEnvVar("VITE_APP_SERVER_WS_URI");
export const SERVER_STATIC_URI = getEnvVar("VITE_APP_SERVER_STATIC_URI");

export const LOG_LVL = getEnvVar("VITE_APP_LOG_LVL");

export const APP_TITLE = getEnvVar("VITE_APP_TITLE");
