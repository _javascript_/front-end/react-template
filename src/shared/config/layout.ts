import { LayoutVariants } from "shared/ui";

export const LAYOUT_NAMES = {
  ONLY_CONTENT: LayoutVariants.OnlyContent,
  HEADER: LayoutVariants.Header,
  HEADER_FOOTER: LayoutVariants.HeaderFooter,
  HEADER_LEFTBAR: LayoutVariants.HeaderLeftbar,
  HEADER_LEFTBAR_FOOTER: LayoutVariants.HeaderLeftbarFooter,
};
