import { LOG_LVL } from "shared/config";
import { transform } from "shared/utils/date";

type TypeLVL = "Error" | "Warn" | "Info" | "Debug" | "Trace";

enum LVL {
  Error,
  Warn,
  Info,
  Debug,
  Trace,
}

class Logger {
  private lvl: TypeLVL = "Trace";
  private scope: string;
  constructor(lvl: TypeLVL, scope: string = "") {
    this.lvl = lvl;
    this.scope = scope;
  }

  private nowDays = () => {
    const now = new Date();
    return transform(now, "hh:mm:ss:ms");
  };

  private _log(text: string, extra: string[], data?: object, lvl?: string) {
    let style = Style.base.join(";") + ";";
    style += extra.join(";");

    let msg = `[${this.nowDays()}]`;
    if (lvl) msg += ` | ${lvl.toUpperCase()} |`;
    if (this.scope) msg += ` ${this.scope} ->`;
    if (text) msg += ` ${text}`;

    if (data) console.log(`%c${msg}`, style, data);
    else console.log(`%c${msg}`, style);
  }

  public error(text: string, data?: object) {
    this._log(text, Style.error, data, "Error");
  }
  public warn(text: string, data?: object) {
    if (LVL[this.lvl] >= 1) {
      this._log(text, Style.warn, data, "Warn");
    }
  }
  public info(text: string, data?: object) {
    if (LVL[this.lvl] >= 2) {
      this._log(text, Style.info, data, "Info");
    }
  }
  public debug(text: string, data?: object) {
    if (LVL[this.lvl] >= 3) {
      this._log(text, Style.debug, data, "Debug");
    }
  }
  public log(text: string, data?: object) {
    if (LVL[this.lvl] >= 4) {
      this._log(text, Style.log, data, " Log ");
    }
  }

  public createScope(scope: string) {
    return new Logger(LOG_LVL, scope);
  }
}

const Style = {
  test: [
    "color: red",
    "font-weight: 600",
    "padding: 2px 4px",
    "border-radius: 2px",
  ],
  base: [
    "color: #eee",
    "font-weight: 600",
    "padding: 2px 4px",
    "border-radius: 2px",
  ],
  error: ["background-color: coral"],
  warn: ["color: #444", "background-color: khaki"],
  info: ["background-color: deepskyblue"],
  debug: ["background-color: mediumseagreen"],
  log: ["color: #444", "background-color: lightgray"],
};

export type LoggerType = Logger;
export const logger = new Logger(LOG_LVL);
