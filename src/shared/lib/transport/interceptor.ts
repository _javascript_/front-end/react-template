import { random } from "shared/utils";

import { Config } from "./http";

type InterceptorRequestFn = (
  config: Partial<Config>
) => Partial<Config> | Promise<Partial<Config>>;

type InterceptorResponseFn = (
  response: Response
) => Response | Promise<Response>;

type InterceptorDataFn = (data: any) => any;

type InterceptorError = {
  config: Partial<Config>;
  response: Response;
};
type InterceptorErrorFn = (
  error: InterceptorError
) => InterceptorError | Promise<InterceptorError>;

type InterceptorsRequest = {
  [key: string]: InterceptorRequestFn;
};

type InterceptorsResponse = {
  [key: string]: {
    interceptorResponse: InterceptorResponseFn;
    interceptorError: InterceptorErrorFn;
  };
};

type InterceptorsData = {
  [key: string]: InterceptorDataFn;
};

type HttpInterceptorDTO = {
  interceptorsRequest?: InterceptorsRequest;
  interceptorsResponse?: InterceptorsResponse;
  interceptorsData?: InterceptorsData;
};

class HttpInterceptors {
  private interceptorsRequest: InterceptorsRequest = {};
  private interceptorsResponse: InterceptorsResponse = {};
  private interceptorsData: InterceptorsData = {};

  constructor(dto?: HttpInterceptorDTO) {
    this.interceptorsRequest = dto?.interceptorsRequest ?? {};
    this.interceptorsResponse = dto?.interceptorsResponse ?? {};
  }

  public interceptorsRequestNotEmpty() {
    return Boolean(Object.keys(this.interceptorsRequest).length);
  }

  public interceptorsResponseNotEmpty() {
    return Boolean(Object.keys(this.interceptorsResponse).length);
  }

  public interceptorsDataNotEmpty() {
    return Boolean(Object.keys(this.interceptorsData).length);
  }

  static merge(interceptors: HttpInterceptors) {
    return new HttpInterceptors({
      interceptorsRequest: { ...interceptors.interceptorsRequest },
      interceptorsResponse: { ...interceptors.interceptorsResponse },
    });
  }

  public request() {
    return {
      use: (interceptor: InterceptorRequestFn) => {
        const key = this.createKey();
        this.interceptorsRequest[key] = interceptor;
        return key;
      },
      eject: (interceptor: string) => {
        delete this.interceptorsRequest[interceptor];
      },
    };
  }

  public response() {
    return {
      use: (
        interceptorResponse: InterceptorResponseFn,
        interceptorError: InterceptorErrorFn
      ) => {
        const key = this.createKey();
        this.interceptorsResponse[key] = {
          interceptorResponse,
          interceptorError,
        };
        return key;
      },
      eject: (interceptor: string) => {
        delete this.interceptorsResponse[interceptor];
      },
    };
  }

  async interceptorsRequestStart(
    config: Partial<Config>
  ): Promise<Partial<Config>> {
    for (const interceptor of Object.values(this.interceptorsRequest)) {
      config = await interceptor(config);
    }
    return config;
  }

  async interceptorsResponseStart(
    response: Response,
    config: Partial<Config>
  ): Promise<Response> {
    try {
      for (const { interceptorResponse } of Object.values(
        this.interceptorsResponse
      )) {
        if (response.status >= 300) throw new Error();
        response = await interceptorResponse(response);
      }
    } catch {
      const { response: res } = await this.interceptorsErrorStart({
        config,
        response,
      });
      response = res;
    }
    return response;
  }

  async interceptorsErrorStart(
    error: InterceptorError
  ): Promise<InterceptorError> {
    for (const { interceptorError } of Object.values(
      this.interceptorsResponse
    )) {
      error = await interceptorError(error);
    }
    return error;
  }

  private createKey() {
    return random.generateToken();
  }

  public data() {
    return {
      use: (interceptor: InterceptorDataFn) => {
        const key = this.createKey();
        this.interceptorsData[key] = interceptor;
        return key;
      },
      eject: (interceptor: string) => {
        delete this.interceptorsData[interceptor];
      },
    };
  }

  async interceptorsDataStart(data: any) {
    for (const interceptor of Object.values(this.interceptorsData)) {
      data = await interceptor(data);
    }
    return data;
  }
}

export { HttpInterceptors };
