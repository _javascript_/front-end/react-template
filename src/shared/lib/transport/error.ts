type Status =
  | "success"
  | "client-error"
  | "server-error"
  | "redirect"
  | "information";

type HttpErrorDTO = {
  status?: Status;
  message?: string;
  code?: number;
  errors?: any;
};

export class HttpError extends Error {
  public status?: Status;
  public code?: number;
  public errors?: any;

  constructor(dto: HttpErrorDTO) {
    super(dto.message);
    this.status = dto.status;
    this.code = dto.code;
    this.errors = dto.errors;
  }

  static getStatus(code: number): Status {
    if (code >= 100 && code < 200) return "information";
    if (code >= 200 && code < 300) return "success";
    if (code >= 300 && code < 400) return "redirect";
    if (code >= 400 && code < 500) return "client-error";
    return "server-error";
  }

  static getMessage(code: number): string {
    return STATUS_MESSAGE[code] ?? this.getStatus(code).toUpperCase();
  }
}

export const STATUS_MESSAGE: { [key: number]: string } = {
  200: "OK",
  201: "Create",
  202: "Accepted",
  203: "Non-Authoritative Information",
  204: "No Content",
  400: "Bad Request",
  401: "Unauthorized",
  403: "Forbidden",
  404: "Not Found",
  500: "Internal Server Error",
};
