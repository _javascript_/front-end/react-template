import { HttpTransport } from "./http";

export abstract class Entity {
  protected log = () => console.log(this);
  static new = <Instance extends Entity, DTO>(
    entity: new (dto: DTO) => Instance,
    dto: DTO
  ): Instance => new entity(dto);
  static list = <Instance extends Entity, DTO>(
    entity: new (dto: DTO) => Instance,
    dtoList: DTO[]
  ): Instance[] => dtoList.map((dto) => new entity(dto));
}

export class Repository<E extends Entity> {
  protected fetch: HttpTransport;
  protected baseUrl: string;
  protected entity?: new (dto: E) => E;

  constructor(
    transport: HttpTransport,
    url: string,
    entity?: new (dto: E) => E
  ) {
    this.fetch = transport;
    this.baseUrl = url;
    this.entity = entity;
  }

  protected async createEntity<Dto>(dto: Dto) {
    return await this.fetch.post(this.baseUrl, dto);
  }

  protected async findAllEntitys() {
    const result = await this.fetch.get(this.baseUrl);
    if (!this.entity) return result as E[];
    return Entity.list(this.entity, result);
  }

  protected async findOneEntity(id: number) {
    const result = await this.fetch.get(this.baseUrl + `/${id}`);
    if (!this.entity) return result as E;
    return Entity.new(this.entity, result);
  }

  protected updateEntity<Dto>(id: number, dto: Dto) {
    return this.fetch.put(this.baseUrl + `/${id}`, dto);
  }

  protected removeEntity(id: number) {
    return this.fetch.delete(this.baseUrl + `/${id}`);
  }

  protected path(method: string) {
    return "/" + this.baseUrl + `/${method}`;
  }
}
