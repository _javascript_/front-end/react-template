const CALL_TIMEOUT = 7 * 1000;
const PING_INTERVAL = 60 * 1000;
const RECONNECT_TIMEOUT = 2 * 1000;

export class WSTransport {
  protected url: string;
  protected socket: WebSocket | null = null;
  protected active: boolean = false;
  protected connected: boolean = false;
  protected callTimeout: number = CALL_TIMEOUT;
  protected pingInterval: number = PING_INTERVAL;
  protected reconnectTimeout: number = RECONNECT_TIMEOUT;
  protected lastActivity: number;

  constructor(url: string) {
    this.url = url;
    this.lastActivity = new Date().getTime();
  }

  private toID: string | string[] | null = null;
  private events: { event: Function; args: any }[] = [];
  public id: string | null = null;

  async open() {
    if (this.connected) return;
    const socket = new WebSocket(this.url);
    this.active = true;
    this.socket = socket;

    this.socket.addEventListener("open", () => {
      this.connected = true;
      this.executeEventsInTheTurn();
    });

    socket.addEventListener("message", ({ data }) => {
      console.log("WSTransport MESSAGE", data);
    });

    socket.addEventListener("close", () => {
      this.connected = false;
      setTimeout(() => {
        if (this.active) this.open();
      }, this.reconnectTimeout);
    });

    socket.addEventListener("error", (err) => {
      socket.close();
    });

    setInterval(() => {
      if (this.active) {
        const interval = new Date().getTime() - this.lastActivity;
        if (interval > this.pingInterval) this.emit("PING");
      }
    }, this.pingInterval);
  }

  close() {
    this.active = false;
    if (!this.socket) return;
    this.socket.close();
    this.socket = null;
  }

  private addEventToTurn(event: Function, ...args: any) {
    this.events.push({ event, args });
  }

  private executeEventsInTheTurn() {
    this.events.forEach(({ event, args }) => event.apply(this, args));
    this.events = [];
  }

  public emit(type: string, payload: any = {}) {
    if (!this.socket) return;
    if (this.socket.readyState !== this.socket.OPEN) {
      return this.addEventToTurn(this.emit, type, payload);
    }
    if (this.id) payload.from = this.id;
    if (this.toID) payload.to = this.toID;
    const data = JSON.stringify({ type, payload });
    this.socket.send(data);
    this.toID = null;
  }

  public on<T>(type: string, cb: (payload: T) => void) {
    if (!this.socket) return;

    this.socket.addEventListener("message", (event) => {
      const data = JSON.parse(event.data);
      if (type === data?.type) cb(data?.payload);
    });
  }

  public onopen(type: string, payload = {}) {
    if (!this.socket) return;

    this.socket.addEventListener("open", (event) => {
      this.emit(type, payload);
    });
  }

  public onclose(type: string, payload = {}) {
    if (!this.socket) return;

    this.socket.addEventListener("close", (event) => {
      this.emit(type, payload);
    });
  }

  public onerror(type: string, payload = {}) {
    if (!this.socket) return;

    this.socket.addEventListener("error", (event) => {
      this.emit(type, payload);
    });
  }

  public to(to: string | string[]) {
    this.toID = to;
    return this;
  }
}
