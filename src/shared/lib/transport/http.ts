import { logger } from "../logger";
import { HttpError } from "./error";

import { HttpInterceptors } from "./interceptor";

export type Config = Omit<RequestInit, "body"> & {
  baseURL: string;
  url: string;
  timeout: number;
  withCredentials: boolean;
  body?: object;
};

const DEFAULT_CONFIG: Config = {
  baseURL: "",
  url: "",
  withCredentials: false,
  headers: {},
  timeout: 0,
};

class HttpTransport {
  private logger = logger.createScope("HttpTransport");
  private config: Config = DEFAULT_CONFIG;
  private httpInterceptors: HttpInterceptors;

  constructor(config: Partial<Config> = {}, interceptors?: HttpInterceptors) {
    this.config = { ...this.config, ...config };
    if (interceptors) this.httpInterceptors = interceptors;
    else this.httpInterceptors = new HttpInterceptors();
  }

  public create(config: Partial<Config> = {}) {
    const conf = { ...this.config, ...config };
    const interceptors = HttpInterceptors.merge(this.httpInterceptors);
    return new HttpTransport(conf, interceptors);
  }

  public async request(config: Partial<Config>) {
    config = { ...this.config, ...config };

    if (this.httpInterceptors.interceptorsRequestNotEmpty()) {
      config = await this.httpInterceptors.interceptorsRequestStart(config);
    }
    const { baseURL, url, timeout, withCredentials, body, ...options } = config;

    let headers = { ...this.config.headers, ...config.headers };
    if (body) headers = { ...headers, "Content-Type": "application/json" };

    const totalConfig: any = {
      ...options,
      credentials: withCredentials ? "include" : undefined,
      body: body ? JSON.stringify(body) : undefined,
      headers,
    };

    this.logger.debug("Request", totalConfig);

    let response = await fetch(`${baseURL}${url}`, totalConfig);

    if (this.httpInterceptors.interceptorsResponseNotEmpty()) {
      response = await this.httpInterceptors.interceptorsResponseStart(
        response,
        config
      );
    }

    return response;
  }

  private async send(config: Partial<Config>) {
    const response = await this.request(config);
    return await this.message(response);
  }

  public async get(url: string, query?: object) {
    if (query) url += this.querySerializer(query);
    return await this.send({ url, method: "GET" });
  }

  public async post(url: string, body = {}) {
    return await this.send({ url, method: "POST", body });
  }

  public async put(url: string, body = {}) {
    return await this.send({ url, method: "PUT", body });
  }

  public async delete(url: string, body = {}) {
    return await this.send({ url, method: "DELETE", body });
  }

  public get interceptors() {
    return {
      request: this.httpInterceptors.request(),
      response: this.httpInterceptors.response(),
      data: this.httpInterceptors.data(),
    };
  }

  private async message(response: Response) {
    const code = response.status;
    const status = HttpError.getStatus(response.status);
    const message = HttpError.getMessage(response.status);

    const packet = await response.text();
    if (packet === "{}") throw new HttpError({ status, code, message });

    let data = JSON.parse(packet);
    const isCustomError = Boolean(data?.errors);

    if (isCustomError || code >= 400) {
      this.logger.error("Response", data);
      throw new HttpError({ status, code, message, ...data });
    }
    this.logger.debug("Response", data);

    if (this.httpInterceptors.interceptorsDataNotEmpty()) {
      data = this.httpInterceptors.interceptorsDataStart(data);
    }

    return data;
  }

  private querySerializer(query: object = {}): string {
    return Object.entries(query).reduce((acc, [key, value], index) => {
      if (value) acc += `${index ? "&" : "?"}${key}=${value}`;
      return acc;
    }, "");
  }
}

export { HttpTransport };
