import { ACTIONS } from "./ws_actions";

export type ActionsType = typeof ACTIONS[keyof typeof ACTIONS];
export type Payload<T> = T;
export type WSData<T> = {
  type: ActionsType;
  payload: T;
};
export type InitPayload = {
  id: string;
};
export type SayHiPayload = {
  to: string;
  from: string;
  message: string;
};
export type HandlerJoinPayload = {
  to: string;
  from: string;
  message: string;
  roomUsers: string[];
};
