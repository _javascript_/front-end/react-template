import { HttpTransport } from "./http";
import { Repository } from "./repo";
import * as error from "./error";

const $fetch = new HttpTransport();

export { $fetch, error, Repository };
