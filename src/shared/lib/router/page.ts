import { LayoutVariant } from "shared/ui";

export type PageConfig = {
  pageName: string;
  layout: LayoutVariant;
  path: string;
  access?: {
    allowed?: string[];
    redirect?:
      | {
          [role: string]: string;
        }
      | string;
  };

  component: React.LazyExoticComponent<(props: any) => JSX.Element>;
};
