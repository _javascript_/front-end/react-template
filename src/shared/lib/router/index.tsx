import React from "react";
import { RouteObject } from "react-router";
import { createBrowserHistory } from "history";

import { Loader } from "shared/ui";
import { PageConfig } from "./page";

type MetaDataPage = Omit<PageConfig, "component">;

export type InjectionProps = {
  metadata: MetaDataPage;
};

const history = createBrowserHistory();

function getRouteConfigs(configs: PageConfig[]): RouteConfig[] {
  function getRouteObject(config: PageConfig): RouteObject {
    const { component, ...metadata } = config;
    const { path } = metadata;

    const element = withSuspenseAndMetadata(component, metadata);

    return { path: config.path, element, index: config.path === "/" };
  }

  return configs.map((config) => {
    const route = getRouteObject(config);
    const { component, ...metadata } = config;
    return { route, metadata };
  });
}

export type RouteConfig = {
  route: RouteObject;
  metadata: MetaDataPage;
};

function withSuspenseAndMetadata(
  Component: React.FC<InjectionProps>,
  metadata: MetaDataPage
) {
  return (
    <React.Suspense fallback={<Loader />}>
      <Component metadata={metadata} />
    </React.Suspense>
  );
}

export { history, getRouteConfigs };
