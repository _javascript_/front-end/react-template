import { pages } from "./pages";
import { helmet } from "./helmet";
import { form } from "./form";

export const en = {
  pages,
  helmet,
  form,
};
