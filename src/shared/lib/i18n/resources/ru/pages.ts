export const pages = {
  unprotected: "Не защищённая страница",
  protected: "Защищённая страница",
  sign_in: "Авторизация",
  sign_up: "Регистрация",
  error: "Ошибка",
  admin: "Админка",
};
