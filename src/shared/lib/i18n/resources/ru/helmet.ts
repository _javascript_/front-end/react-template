import { APP_TITLE } from "shared/config";
import { defaultHelmet } from "shared/utils/common";

const helmetWithPrefix = (name: string) => {
  return defaultHelmet(name, APP_TITLE);
};

export const helmet = {
  home: helmetWithPrefix("home"),
  about: helmetWithPrefix("about"),
  protected: helmetWithPrefix("protected"),
  unprotected: helmetWithPrefix("unprotected"),
  sign_in: helmetWithPrefix("sign_in"),
  sign_up: helmetWithPrefix("sign_up"),
  error: helmetWithPrefix("error"),
};
