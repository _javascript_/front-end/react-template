export const required = (value: string = "") =>
  value.trim() ? undefined : "required";

export const isEmail = (value: string = "") => {
  const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  return reg.test(value) ? undefined : "is_not_email";
};
