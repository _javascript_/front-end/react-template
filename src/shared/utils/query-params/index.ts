export const KEY_SORT_BY = "sortBy";
export const KEY_PAGE = "page";
export const KEY_PER_PAGE = "perPage";

export const PAGINATION_SEARCH_PARAMS = [KEY_SORT_BY, KEY_PAGE, KEY_PER_PAGE];

export function get(keys: string[]): any {
  const params = new URLSearchParams(window.location.search);
  const array = keys;

  const query: any = array.reduce<any>((acc, key) => {
    params.forEach((parametrValue, parametrKye) => {
      if (key === parametrKye) {
        acc[key] = parametrValue;
      }
    });
    return acc;
  }, {});
  return query;
}
