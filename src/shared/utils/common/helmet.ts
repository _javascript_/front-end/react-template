export function defaultHelmet(name: string, prefix?: string) {
  return {
    title: `${prefix ? prefix + ": " : ""}${name.toUpperCase()}`,
    description: `Description - ${name}`,
  };
}
