export function renderObject(obj?: object) {
  if (!obj) return null;
  return (
    <ul>
      {Object.entries(obj).map(([key, value]) => {
        if (typeof value === "object") {
          if (Array.isArray(value)) {
            value = value.join(", ");
          } else value = renderObject(value);
        }
        if (typeof value === "boolean") {
          value = value ? "true" : "false";
        }
        return (
          <li key={key}>
            {key}: {value ?? "null"}
          </li>
        );
      })}
    </ul>
  );
}
