type ValidClasses = {
  [className: string]: boolean | undefined | null;
};

const objectToString = (classes: ValidClasses): string => {
  const result = Object.entries(classes).reduce<string[]>((acc, [k, v]) => {
    if (v) acc.push(k);
    return acc;
  }, []);
  return result.join(" ");
};

export function clsx(...classes: (undefined | string | ValidClasses)[]) {
  return classes
    .map((className) => {
      if (typeof className === "object") {
        return objectToString(className);
      }
      if (typeof className === "string") return className;
      return "";
    })
    .join(" ");
}
