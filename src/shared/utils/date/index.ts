const YEAR = "YYYY";
const MONTH = "MM";
const DAY = "DD";
const HOURS = "hh";
const MINUTES = "mm";
const SECONDS = "ss";
const MILLI_SECONDS = "ms";

const hh_mm: TrVariant = `${HOURS}:${MINUTES}`;
const hh_mm_ss: TrVariant = `${HOURS}:${MINUTES}:${SECONDS}`;
const hh_mm_ss_ms: TrVariant = `${HOURS}:${MINUTES}:${SECONDS}:${MILLI_SECONDS}`;
const DD_MM_YYYY: TrVariant = `${DAY}.${MONTH}.${YEAR}`;
const DD_MM_YYYY_hh_mm: TrVariant = `${DD_MM_YYYY} | ${hh_mm}`;

type TrVariant =
  | "DD.MM.YYYY | hh:mm"
  | "DD.MM.YYYY"
  | "hh:mm"
  | "hh:mm:ss"
  | "hh:mm:ss:ms";

export enum TrVariants {
  DD_MM_YYYY_hh_mm = "DD.MM.YYYY | hh:mm",
  DD_MM_YYYY = "DD.MM.YYYY",
  hh_mm = "hh:mm",
  hh_mm_ss = "hh:mm:ss",
  hh_mm_ss_ms = "hh:mm:ss:ms",
}

export const transform = (
  date: number | Date,
  variant: TrVariant,
  notUtc?: boolean
) => {
  if (!variant) return;
  date = date instanceof Date ? date : date * 1000;
  const jsDate = new Date(date);

  const year = notUtc ? jsDate.getFullYear() : jsDate.getUTCFullYear();
  const month = notUtc ? jsDate.getMonth() + 1 : jsDate.getUTCMonth() + 1;
  const day = notUtc ? jsDate.getDate() : jsDate.getUTCDate();
  const hours = notUtc ? jsDate.getHours() : jsDate.getUTCHours();
  const minutes = notUtc ? jsDate.getMinutes() : jsDate.getUTCMinutes();
  const seconds = notUtc ? jsDate.getSeconds() : jsDate.getUTCSeconds();
  const milliSeconds = notUtc
    ? jsDate.getMilliseconds()
    : jsDate.getUTCMilliseconds();

  const MM = month <= 9 ? "0" + month : month;
  const DD = day <= 9 ? "0" + day : day;
  const hh = hours <= 9 ? "0" + hours : hours;
  const mm = minutes <= 9 ? "0" + minutes : minutes;
  const ss = seconds <= 9 ? "0" + seconds : seconds;
  const ms = milliSeconds <= 99 ? "0" + milliSeconds : milliSeconds;

  if (variant === hh_mm) {
    return `${hh}:${mm}`;
  }
  if (variant === hh_mm_ss) {
    return `${hh}:${mm}:${ss}`;
  }
  if (variant === hh_mm_ss_ms) {
    return `${hh}:${mm}:${ss}:${ms}`;
  }
  if (variant === DD_MM_YYYY) {
    return `${DD}.${MM}.${year}`;
  }
  if (variant === DD_MM_YYYY_hh_mm) {
    return `${DD}.${MM}.${year} | ${hh}:${mm}`;
  }
  return "";
};

export const startOfTheDay = (date?: Date) => {
  const d = date ? new Date(date) : new Date();
  d.setHours(0);
  d.setMinutes(0);
  d.setSeconds(0);
  d.setMilliseconds(0);
  return d;
};

export const endOfTheDay = (date?: Date) => {
  const secondsPerDay = 86400;
  const d = date ? new Date(date) : new Date();
  d.setHours(0);
  d.setMinutes(0);
  d.setMilliseconds(0);
  d.setSeconds(secondsPerDay - 1);
  return d;
};

export const trDateInTimestamp = (date: Date) => {
  return date.getTime() / 1000;
};

export const addHours = (date: Date, hours: number): Date => {
  const newDate = new Date(date);
  newDate.setMilliseconds(hours * 60 * 60 * 1000);
  return newDate;
};
