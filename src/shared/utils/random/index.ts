const ALPHA_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
const ALPHA = ALPHA_UPPER + ALPHA_LOWER;
const DIGIT = "0123456789";
const ALPHA_DIGIT = ALPHA + DIGIT;

export function random(min: number = 0, max: number = 10) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomDate(start: Date, end: Date) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  );
}

export function randomEmail() {
  const domains = ["com", "ru", "org", "net", "info"];
  return `${generateToken(random())}@${generateToken(random())}.${
    domains[random(0, domains.length - 1)]
  }`;
}

export function generateToken(tokenLength: number = 10) {
  const base = ALPHA_DIGIT.length;
  let key = "";
  for (let i = 0; i < tokenLength; i++) {
    const index = Math.floor(Math.random() * base);
    key += ALPHA_DIGIT[index];
  }
  return key;
}

export function ucFirst(str?: string) {
  if (!str) return str;
  return str[0].toUpperCase() + str.slice(1);
}

export const generateFio = () => {
  const surnameLenght = random(3, 8);
  const surname = generateToken(surnameLenght);
  const name = ALPHA[random(1, ALPHA.length)];
  const lastName = ALPHA[random(1, ALPHA.length)];
  return `${ucFirst(surname)} ${ucFirst(name)}.${ucFirst(lastName)}.`;
};
