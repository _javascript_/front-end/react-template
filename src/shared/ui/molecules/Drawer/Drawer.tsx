import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { clsx } from "shared/utils";
import { DrawerProps } from "./types";

import { Backdrop } from "shared/ui/atoms";
import "./Drawer.css";

export const Drawer = ({
  children,
  onClose,
  open,
  anchor = "right",
}: DrawerProps) => {
  const [mount, setMount] = useState<boolean>(false);

  useEffect(() => {
    if (open) setMount(true);
    else setTimeout(() => setMount(false), 300);
  }, [open]);

  if (!mount) return null;

  return ReactDOM.createPortal(
    <div
      className={clsx("drawer", {
        drawer_close: !open,
      })}
    >
      <Backdrop onClick={onClose} />
      <div
        className={clsx("drawer__content", {
          drawer__content_left: anchor === "left",
          drawer__content_right: anchor === "right",
          drawer__content_top: anchor === "top",
          drawer__content_bottom: anchor === "bottom",
        })}
      >
        {children}
      </div>
    </div>,
    document.body
  );
};
