import { ReactNode } from "react";

export type DrawerProps = {
  children?: ReactNode;
  open?: boolean;
  onClose?: () => void;
  anchor?: "left" | "right" | "top" | "bottom";
};
