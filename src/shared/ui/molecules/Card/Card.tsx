import React from "react";
import {
  CardProps,
  CardBodyProps,
  CardFooterProps,
  CardHeaderProps,
} from "./types";

import "./Card.css";
import { clsx } from "shared/utils";

export const Card = ({ children, className, onClick }: CardProps) => {
  return (
    <div className={clsx("card", className)} onClick={onClick}>
      {children}
    </div>
  );
};

export const CardHeader = ({ children, className }: CardHeaderProps) => {
  return (
    <div className={clsx("card__header", "card__element", className)}>
      {children}
    </div>
  );
};

export const CardFooter = ({ children, className }: CardFooterProps) => {
  return (
    <div className={clsx("card__footer", "card__element", className)}>
      {children}
    </div>
  );
};

export const CardBody = ({ children, className }: CardBodyProps) => {
  return (
    <div className={clsx("card__body", "card__element", className)}>
      {children}
    </div>
  );
};
