import { ReactNode } from "react";

export type CardProps = {
  children?: ReactNode;
  className?: string;
  onClick?: () => void;
};

export type CardFooterProps = {
  className?: string;
  children?: ReactNode;
};

export type CardBodyProps = {
  children?: ReactNode | ReactNode[];
  className?: string;
};

export type CardHeaderProps = {
  children?: ReactNode;
  className?: string;
};
