import { ReactNode, CSSProperties } from "react";

export type PageProps = {
  children: ReactNode;
  helmet?: {
    title?: string;
    description?: string;
  };
};
