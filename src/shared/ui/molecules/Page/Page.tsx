import React from "react";
import { Helmet } from "react-helmet-async";

import { PageProps } from "./types";

export const Page: React.FC<PageProps> = ({ children, helmet }) => {
  return (
    <>
      <Helmet>
        {helmet?.title && <title>{helmet.title}</title>}
        {helmet?.description && (
          <meta name="description" content={helmet.description} />
        )}
      </Helmet>
      {children}
    </>
  );
};
