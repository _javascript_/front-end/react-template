import React from "react";

import "./Loader.css";

export type Props = {};

export const Loader = (props: Props) => {
  return (
    <div className="loader__container">
      <h3 className="loader">Loading...</h3>
    </div>
  );
};
