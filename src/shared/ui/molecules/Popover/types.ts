import { ReactNode } from "react";

export type AnchorOrigin = {
  vertical: "bottom" | "top" | "center";
  horizontal: "left" | "right" | "center";
};

export type PopoverProps = {
  children?: ReactNode;
  anchorEl?: HTMLElement | null;
  onClose?: () => void;
  open?: boolean;
  anchorOrigin?: {
    vertical: AnchorOrigin["vertical"];
    horizontal: AnchorOrigin["horizontal"];
  };
};
