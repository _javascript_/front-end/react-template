import React, { useEffect, useState } from "react";
import { AnchorOrigin, PopoverProps } from "./types";

import "./Popover.css";
import ReactDOM from "react-dom";
import { clsx } from "shared/utils";
import { Backdrop } from "shared/ui/atoms";

export const Popover = (props: PopoverProps) => {
  const {
    anchorEl = null,
    anchorOrigin = { horizontal: "center", vertical: "bottom" },
    children,
    onClose,
    open = false,
  } = props;

  const [mount, setMount] = useState<boolean>(false);
  const [react, setReact] = useState<DOMRect | null>(null);

  useEffect(() => {
    if (open) setMount(true);
    if (anchorEl) setReact(anchorEl.getBoundingClientRect());
    if (!open || !anchorEl)
      setTimeout(() => {
        setMount(false);
        setReact(null);
      }, 300);
  }, [open, anchorEl]);

  if (!mount || !react) return null;

  const { x, y } = calculatePosition(react, anchorOrigin);

  return ReactDOM.createPortal(
    <div className={clsx("popover", { popover_close: !open })}>
      <Backdrop transparent onClick={onClose} />
      <div
        className={clsx("popover__content")}
        style={{
          top: y,
          left: x,
        }}
      >
        {children}
      </div>
    </div>,
    document.body
  );
};

const calculatePosition = (rect: DOMRect, anchorOrigin: AnchorOrigin) => {
  let { height, width, x, y } = rect;
  if (anchorOrigin.horizontal === "right") x = x + width;
  if (anchorOrigin.horizontal === "left") x = x - width;
  if (anchorOrigin.vertical === "bottom") y = y + height + 4;
  if (anchorOrigin.vertical === "top") y = y - height;

  return { x, y };
};
