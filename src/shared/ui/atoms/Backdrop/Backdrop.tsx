import React from "react";
import { clsx } from "shared/utils";

import { BackdropProps } from "./types";
import "./Backdrop.css";

export const Backdrop = ({ children, onClick, transparent }: BackdropProps) => {
  return (
    <div
      className={clsx("backdrop", { backdrop_transparent: transparent })}
      onClick={onClick}
    >
      {children}
    </div>
  );
};
