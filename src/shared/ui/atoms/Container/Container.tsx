import React from "react";
import { ContainerProps } from "./types";

import "./Container.css";
import { clsx } from "shared/utils";

export const Container = (props: ContainerProps) => {
  const { tag = "div" } = props;
  return React.createElement(tag, {
    className: clsx(
      "container",
      {
        container_center: props.variant === "center",
        container_fullscreen: props.variant === "fullscreen",
      },
      props.className
    ),
    children: props.children,
  });
};
