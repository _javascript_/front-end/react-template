import { ReactHTML, ReactNode } from "react";

export type ContainerVariant = "fullscreen" | "center";

export type ContainerProps = {
  tag?: keyof ReactHTML;
  variant?: ContainerVariant;
  children?: ReactNode;
  className?: string;
};
