import React from "react";
import { InputProps } from "./types";

import "./Input.css";
import { clsx } from "shared/utils";

export const Input = ({
  value,
  onChange,
  className,
  placeholder,
  label,
  name,
}: InputProps) => {
  return (
    <label className="input__label">
      {label}
      <input
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        className={clsx("input", className)}
      />
    </label>
  );
};
