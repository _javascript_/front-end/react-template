import { ReactNode } from "react";

export type InputProps = {
  label?: string;
  name?: string;
  placeholder?: string;
  className?: string;
  value?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
};
