import { ReactNode } from "react";

export type ButtonProps = {
  /**
   * Размер кнопки
   */
  size?: "small" | "medium" | "large";
  /**
   * class
   */
  className?: string;
  /**
   * Контент кнопки
   */
  children?: ReactNode;
  /**
   * Событие клик
   */
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  /**
   * Кнопка ссылкак
   */
  to?: string;
  /**
   * Вариант цвета
   */
  color?: "primary" | "secondary" | "default";
  /**
   *  На всё ширину
   */
  fullWidth?: boolean;
};
