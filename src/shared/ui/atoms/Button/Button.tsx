import React from "react";
import { Link } from "react-router-dom";
import { clsx } from "shared/utils";

import { ButtonProps } from "./types";
import "./Button.css";

/**
 * UI component Button
 */
export const Button = (props: ButtonProps) => {
  const {
    children,
    onClick,
    className,
    to,
    size = "medium",
    color = "default",
    fullWidth = false,
  } = props;

  const classes = clsx(
    "button",
    `button_size_${size}`,
    `button_color_${color}`,
    {
      "button_full-width": fullWidth,
    },
    className
  );

  if (to) {
    return (
      <Link to={to} className={classes}>
        {children}
      </Link>
    );
  }
  return (
    <button onClick={onClick} className={classes}>
      {children}
    </button>
  );
};
