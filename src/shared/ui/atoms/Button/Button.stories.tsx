import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Button } from "./Button";
import "../../../assets/styles/index.css";
import "./Button.css";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Компоненты/Atoms/Button",
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

const children = "Это кнопка";

export const Medium = Template.bind({});
Medium.args = {
  size: "medium",
  children,
};

export const Small = Template.bind({});
Small.args = {
  size: "small",
  children,
};

export const Large = Template.bind({});
Large.args = {
  size: "large",
  children,
};

export const Primary = Template.bind({});
Primary.args = {
  color: "primary",
  children,
};

export const Secondary = Template.bind({});
Secondary.args = {
  color: "secondary",
  children,
};

export const FullWidth = Template.bind({});
FullWidth.args = {
  fullWidth: true,
  children,
};
