import React from "react";
import { Outlet } from "react-router";

import { IS_DEV_ENV } from "shared/config";

import { LayoutProps } from "./types";

import "./Layout.css";
import { clsx } from "shared/utils";

export const Layout: React.FC<LayoutProps> = ({
  header,
  footer,
  leftbar,
  breadcrumbs,
  developer,
  variant,
}) => {
  const isHeader = variant?.includes("header");
  const isLeftbar = variant?.includes("leftbar");
  const isFooter = variant?.includes("footer");

  return (
    <>
      {IS_DEV_ENV && developer}
      {isHeader && header}
      {isLeftbar && leftbar}
      <main
        className={clsx("main", {
          main__shift: isLeftbar,
        })}
      >
        {/* {breadcrumbs} */}
        <Outlet />
      </main>
      {isFooter && footer}
    </>
  );
};
