export type LayoutVariant =
  | "header"
  | "header-footer"
  | "header-leftbar"
  | "header-leftbar-footer"
  | "only-content";

export enum LayoutVariants {
  Header = "header",
  HeaderFooter = "header-footer",
  HeaderLeftbar = "header-leftbar",
  HeaderLeftbarFooter = "header-leftbar-footer",
  OnlyContent = "only-content",
}

export type LayoutProps = {
  header?: React.ReactElement;
  footer?: React.ReactElement;
  leftbar?: React.ReactElement;
  breadcrumbs?: React.ReactElement;
  developer?: React.ReactElement;
  variant?: LayoutVariant;
};
