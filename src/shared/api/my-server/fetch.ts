import { SERVER_API_URI } from "shared/config";
import { $fetch } from "shared/lib";

export const customFetch = $fetch.create({
  withCredentials: true,
  baseURL: SERVER_API_URI,
});
