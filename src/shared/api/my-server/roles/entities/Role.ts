import { RoleValue } from "shared/api/common";
import { Entity } from "shared/lib/transport/repo";
import { RoleDto } from "../dto";

export class Role extends Entity implements RoleDto {
  value: RoleValue;
  description: string;
  constructor(dto: RoleDto) {
    super();
    this.value = dto.value;
    this.description = dto.description;
  }
}
