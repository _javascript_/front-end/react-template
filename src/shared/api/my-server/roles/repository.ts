import { customFetch } from "..";
import { Repository } from "shared/lib";
import { Role } from "./entities/Role";
import { CreateRoleDto, UpdateRoleDto } from "./dto";

class RoleRepository extends Repository<Role> {
  constructor() {
    super(customFetch, "/roles", Role);
  }

  create(dto: CreateRoleDto) {
    return super.createEntity(dto);
  }

  findAll() {
    return super.findAllEntitys();
  }

  findOne(id: number) {
    return super.findOneEntity(id);
  }

  update(id: number, dto: UpdateRoleDto) {
    return super.updateEntity(id, dto);
  }

  remove(id: number) {
    return super.removeEntity(id);
  }
}

export const repo = new RoleRepository();
