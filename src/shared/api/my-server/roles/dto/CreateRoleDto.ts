import { RoleValue } from "shared/api/common";

export type CreateRoleDto = {
  value: RoleValue;
  description: string;
};
