import { RoleValue } from "shared/api/common";

export type RoleDto = {
  value: RoleValue;
  description: string;
};
