import { CreateRoleDto } from "./CreateRoleDto";

export type UpdateRoleDto = Partial<CreateRoleDto> & {};
