import { AccessToken } from "./auth/entities";
import { repo } from "./auth/repository";

import { customFetch } from "./fetch";

customFetch.interceptors.data.use((packet) => {
  if (packet.data) {
    return packet.data;
  }
  return packet;
});

customFetch.interceptors.request.use(async (config) => {
  const token = AccessToken.get();
  if (token) {
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${token.accessToken}`,
    };
  }
  return config;
});

customFetch.interceptors.response.use(
  (config) => config,
  async (error) => {
    if (error.response.status === 401) {
      const token = await repo.refresh();

      if (token.accessToken) {
        AccessToken.save(token);
        const response = await customFetch.request(error.config);
        return { config: error.config, response };
      }
      return error;
    } else {
      return error;
    }
  }
);
