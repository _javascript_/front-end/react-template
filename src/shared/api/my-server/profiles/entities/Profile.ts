import { Gender } from "shared/api/common";
import { Entity } from "shared/lib/transport/repo";
import { ProfileDto } from "../dto";

export class Profile extends Entity implements ProfileDto {
  firstName: string | null;
  lastName: string | null;
  patronymic: string | null;
  telephone: string | null;
  gender: Gender | null;
  constructor(dto: ProfileDto) {
    super();
    this.firstName = dto.firstName;
    this.lastName = dto.lastName;
    this.patronymic = dto.patronymic;
    this.telephone = dto.telephone;
    this.gender = dto.gender;
  }
  static default() {
    return new Profile({
      firstName: "Ананим",
      lastName: "Ананимович",
      patronymic: "Ананимов",
      telephone: "8 800 555 3535",
      gender: Gender.MALE,
    });
  }
}
