import { customFetch } from "..";
import { Repository } from "shared/lib";
import { Profile } from "./entities/Profile";
import { CreateProfileDto, UpdateProfileDto } from "./dto";

class ProfileRepository extends Repository<Profile> {
  constructor() {
    super(customFetch, "/profiles", Profile);
  }

  create(dto: CreateProfileDto) {
    return super.createEntity(dto);
  }

  findAll() {
    return super.findAllEntitys();
  }

  findOne(id: number) {
    return super.findOneEntity(id);
  }

  update(id: number, dto: UpdateProfileDto) {
    return super.updateEntity(id, dto);
  }

  remove(id: number) {
    return super.removeEntity(id);
  }
}

export const repo = new ProfileRepository();
