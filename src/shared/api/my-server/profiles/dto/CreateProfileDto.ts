import { Gender } from "shared/api/common";

export type CreateProfileDto = {
  firstName: string | null;
  lastName: string | null;
  patronymic: string | null;
  telephone: string | null;
  gender: Gender | null;
};
