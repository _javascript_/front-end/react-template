import { CreateProfileDto } from "./CreateProfileDto";

export type UpdateProfileDto = Partial<CreateProfileDto> & {};
