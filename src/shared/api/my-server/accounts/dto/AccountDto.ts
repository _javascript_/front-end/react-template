import { RoleValue } from "shared/api/common/roles";
import { Profile } from "../../profiles";

export type AccountDto = {
  id: number;
  email: string;
  roles: RoleValue[];
  profile: Profile;
};
