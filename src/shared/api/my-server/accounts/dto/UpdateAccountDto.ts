import { CreateAccountDto } from "./CreateAccountDto";

export type UpdateAccountDto = Partial<CreateAccountDto> & {};
