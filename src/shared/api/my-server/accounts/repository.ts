import { customFetch } from "..";
import { Repository } from "shared/lib";
import { Account } from "./entities/Account";
import { CreateAccountDto, UpdateAccountDto } from "./dto";

class AccountRepository extends Repository<Account> {
  constructor() {
    super(customFetch, "/uccounts", Account);
  }

  create(dto: CreateAccountDto) {
    return super.createEntity(dto);
  }

  findAll() {
    return super.findAllEntitys();
  }

  findOne(id: number) {
    return super.findOneEntity(id);
  }

  update(id: number, dto: UpdateAccountDto) {
    return super.updateEntity(id, dto);
  }

  remove(id: number) {
    return super.removeEntity(id);
  }
}

export const repo = new AccountRepository();
