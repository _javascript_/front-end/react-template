import { RoleValue } from "shared/api/common/roles";
import { Entity } from "shared/lib/transport/repo";

import { Profile } from "../../profiles";

import { AccountDto } from "../dto";

export class Account extends Entity implements AccountDto {
  id: number;
  email: string;
  roles: RoleValue[];
  profile: Profile;
  constructor(dto: AccountDto) {
    super();
    this.id = dto.id;
    this.email = dto.email;
    this.roles = dto.roles;
    this.profile = dto.profile;
  }

  static default() {
    return new Account({
      id: 777,
      email: "default@mail.com",
      profile: Profile.default(),
      roles: ["GUEST"],
    });
  }
}
