import jwt from "jwt-decode";
import { Account } from "../../accounts";

export class AccessToken {
  private static key = "access-token";
  public accessToken: string;

  private constructor(token: string) {
    this.accessToken = token;
  }

  static new(token: string) {
    return new AccessToken(token);
  }

  public decode() {
    return jwt<Account>(this.accessToken);
  }

  static save(token: string | AccessToken) {
    if (token instanceof AccessToken) {
      token = token.accessToken;
    }
    localStorage.setItem(this.key, token);
  }

  static get() {
    const token = localStorage.getItem(this.key);
    if (token) return new AccessToken(token);
    return null;
  }
}
