import { CreateAccountDto } from "../../accounts/dto/CreateAccountDto";

export type SignUpDto = CreateAccountDto;
