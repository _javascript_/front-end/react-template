import { CreateAccountDto } from "../../accounts/dto/CreateAccountDto";

export type SignInDto = CreateAccountDto;
