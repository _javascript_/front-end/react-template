import { SERVER_API_URI } from "shared/config";
import { $fetch, Repository } from "shared/lib";

import { customFetch } from "../fetch";

import { SignInDto } from "./dto/SignInDto";
import { SignUpDto } from "./dto/SignUpDto";
import { AccessToken } from "./entities/AccessToken";

const METHOD = {
  SIGN_IN: "sign_in",
  SIGN_UP: "sign_up",
  SIGN_OUT: "sign_out",
  REFRESH: "refresh",
};

class AuthRepository extends Repository<any> {
  constructor() {
    super(customFetch, "auth");
  }

  async signIn(dto: SignInDto): Promise<AccessToken> {
    const data = await this.fetch.post(this.path(METHOD.SIGN_IN), dto);
    return AccessToken.new(data.accessToken);
  }

  async signUp(dto: SignUpDto): Promise<AccessToken> {
    const data = await this.fetch.post(this.path(METHOD.SIGN_UP), dto);
    return AccessToken.new(data.accessToken);
  }

  signOut(): Promise<void> {
    return this.fetch.get(this.path(METHOD.SIGN_OUT));
  }

  async refresh(): Promise<AccessToken> {
    const response = await $fetch.request({
      url: SERVER_API_URI + this.path(METHOD.REFRESH),
      withCredentials: true,
    });
    const result = await response.json();
    if (!result?.data?.accessToken) throw new Error("not auth");
    return AccessToken.new(result.data.accessToken);
  }
}

export const repo = new AuthRepository();
