import { JSONPLACEHOLDER_API_URI } from "shared/config";
import { $fetch } from "shared/lib";

export const customFetch = $fetch.create({
  baseURL: JSONPLACEHOLDER_API_URI,
});
