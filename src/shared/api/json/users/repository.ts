import { customFetch } from "..";
import { Repository } from "shared/lib";
import { User } from "./entities/User";
import { CreateUserDto, UpdateUserDto } from "./dto";

class UserRepository extends Repository<User> {
  constructor() {
    super(customFetch, "/users", User);
  }

  create(dto: CreateUserDto) {
    return super.createEntity(dto);
  }

  findAll() {
    return super.findAllEntitys();
  }

  findOne(id: number) {
    return super.findOneEntity(id);
  }

  update(id: number, dto: UpdateUserDto) {
    return super.updateEntity(id, dto);
  }

  remove(id: number) {
    return super.removeEntity(id);
  }
}

export const repo = new UserRepository();
