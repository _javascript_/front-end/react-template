import { Entity } from "shared/lib/transport/repo";
import { UserDto } from "../dto";

export class User extends Entity implements UserDto {
  id: number;
  username: string;
  name: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: { lat: string; lng: string };
  };
  phone: string;
  website: string;
  company: { name: string; catchPhrase: string; bs: string };

  constructor(dto: UserDto) {
    super();
    this.id = dto.id;
    this.username = dto.username;
    this.name = dto.name;
    this.email = dto.email;
    this.address = dto.address;
    this.phone = dto.phone;
    this.website = dto.website;
    this.company = dto.company;
  }
}
