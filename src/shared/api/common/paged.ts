export interface PageMeta {
  currentPage: number;
  pageSize: number;
  pageItems: number;
  totalPages: number;
  totalCount: number;
}

export interface PageQuery {
  page?: number;
  perPage?: number;
}

export interface Paged<Entitie> {
  pageMeta: PageMeta;
  elements: Entitie[];
}
