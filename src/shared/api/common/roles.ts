export enum RoleEnum {
  ADMIN = "ADMIN",
  GUEST = "GUEST",
  USER = "USER",
}

export type RoleValue = "ADMIN" | "GUEST" | "USER";
