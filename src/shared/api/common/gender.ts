export enum Gender {
  MALE = "male",
  FEMALE = "female",
  Other = "other",
}
