import { useContext, useEffect } from "react";

import { appContextUtils, appContextConfig } from "features/app-context";
import { logger } from "shared/lib";

export async function useInit() {
  const context = useContext(appContextConfig.ApplicationContext);

  useEffect(() => {
    if (appContextUtils.isAppReady(context)) return;
    logger.info("Application init");
    async function init() {
      try {
        context.actions.auth.refresh();
        logger.info("Application auth");
      } catch (error: any) {
        logger.info("Application not auth", error);
      }
    }
    init().finally(() => context.actions.ctx.set("initialization", false));
  }, []);
}
