import React from "react";
import { Button } from "shared/ui";

import { ProfileMenuProps } from "./types";
import "./ProfileMenu.css";

export const ProfileMenu = ({ onClick }: ProfileMenuProps) => {
  return (
    <div className="profile__menu">
      <Button className="profile__menu-btn" onClick={onClick}>
        <ProfileMenuIcon />
        Menu
      </Button>
    </div>
  );
};

export const ProfileMenuIcon = () => {
  return <span className="profile__menu-icon" />;
};
