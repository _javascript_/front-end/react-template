export type ProfileMenuProps = {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
};
