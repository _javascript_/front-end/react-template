import React from "react";
import { ProfileIconProps } from "./types";

import "./ProfileIcon.css";

export const ProfileIcon = (props: ProfileIconProps) => {
  return (
    <div className="profile__icon-container">
      <div className="profile__icon"></div>
    </div>
  );
};
