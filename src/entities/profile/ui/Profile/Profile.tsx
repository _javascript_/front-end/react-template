import React from "react";
import { ProfileProps } from "./types";

import { ProfileMenu } from "../ProfileMenu";
import { ProfileInfo } from "../ProfileInfo";

import "./Profile.css";
import { Card, CardBody, CardHeader, Popover } from "shared/ui";
import { clsx } from "shared/utils";

export const Profile = ({ children, email, fio, className }: ProfileProps) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  return (
    <div className={clsx("profile", className)}>
      <ProfileMenu onClick={handleClick} />
      <Popover open={open} onClose={handleClose} anchorEl={anchorEl}>
        <Card>
          <CardHeader>
            <ProfileInfo fio={fio} email={email} />
          </CardHeader>
          <CardBody>{children}</CardBody>
        </Card>
      </Popover>
    </div>
  );
};
