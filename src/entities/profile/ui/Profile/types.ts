import { ReactNode } from "react";

export type ProfileProps = {
  className?: string;
  children: ReactNode;
  email: string;
  fio: string;
};
