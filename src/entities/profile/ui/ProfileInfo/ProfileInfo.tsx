import React from "react";
import { ProfileInfoProps } from "./types";

import "./ProfileInfo.css";

export const ProfileInfo = ({ email, fio }: ProfileInfoProps) => {
  return (
    <div className="profile__info">
      <p className="profile__email">{email}</p>
      <p className="profile__fio">{fio}</p>
    </div>
  );
};
