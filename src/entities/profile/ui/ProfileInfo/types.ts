export type ProfileInfoProps = {
  email: string;
  fio: string;
};
