import React, { useState } from "react";

import { json } from "shared/api";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Input,
} from "shared/ui";
import { common, clsx } from "shared/utils";

import "./Users.css";

type Props = {
  users: json.users.User[];
  select: (id: number) => void;
  delete: (id: number) => void;
};

export const Users = (props: Props) => {
  const { select, users } = props;
  return (
    <>
      <div className={clsx("json-users")}>
        {users.map((user) => (
          <User
            key={user.id}
            user={user}
            update={() => console.log(user.id)}
            delete={() => props.delete(user.id)}
            select={() => select(user.id)}
          />
        ))}
      </div>
      <Button onClick={() => console.log("OPEN POPUP")}>Создать</Button>
    </>
  );
};

type UserProps = {
  user: json.users.User;
  update: () => void;
  delete: () => void;
  select: () => void;
};

const User = (props: UserProps) => {
  const { user, update, select } = props;
  return (
    <Card>
      <CardHeader>
        {user.email} {user.username} {user.id}
      </CardHeader>
      <CardBody>{common.renderObject(user)}</CardBody>
      <CardFooter>
        <Button onClick={props.delete}>Удалить</Button>
        <Button onClick={update}>Изменить</Button>
      </CardFooter>
    </Card>
  );
};

type Field = {
  label?: string;
  placeholder?: string;
  name: string;
  validationSchema?: ((v: string) => string | undefined)[];
};

type FormProps = {
  title: string;
  submit: {
    label: string;
    action: (result: any) => void;
  };
  fields: Field[];
  initialState: { [key: string]: string } | null;
};
function Form({ submit, title, initialState, fields }: FormProps) {
  const { state, onChange } = useForm(initialState);

  return (
    <Card>
      <CardHeader>{title}</CardHeader>
      <CardBody>
        {fields.map(({ validationSchema, ...field }) => {
          return (
            <Input
              label={field.label}
              name={field.name as string}
              placeholder={field.placeholder}
              onChange={onChange}
            />
          );
        })}
      </CardBody>
      <CardFooter>
        <Button onClick={() => submit.action(state)} color="primary">
          {submit.label}
        </Button>
      </CardFooter>
    </Card>
  );
}

function useForm(initialState: { [key: string]: string } | null) {
  const [state, setState] = useState(initialState ?? {});

  function onChange(event: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = event.target;
    if (!name) return;
    setState((prev) => ({ ...prev, [name]: value }));
  }

  return { state, onChange };
}
