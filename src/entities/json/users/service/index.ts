import { createDomain, forward } from "effector";

import { json } from "shared/api";

const domain = createDomain("users");

const $users = domain.createStore<json.users.User[]>([]);
const getUsersFx = domain.createEffect(async () => {
  return json.users.repo.findAll();
});
const resetUsers = domain.createEvent();

$users.on(getUsersFx.doneData, (_, users) => users);
$users.reset(resetUsers);

const $selectedUser = domain.createStore<json.users.User | null>(null);
const selectUserId = domain.createEvent<number>();
const getUserFx = domain.createEffect((id: number) => {
  return json.users.repo.findOne(id);
});
const resetSelectedUser = domain.createEvent();

$selectedUser.on(getUserFx.doneData, (_, user) => user);
$selectedUser.reset(resetSelectedUser);
forward({
  from: selectUserId,
  to: getUserFx,
});

const updateUserFx = domain.createEffect(
  (id: number, dto: json.users.UpdateUserDto) => {
    return json.users.repo.update(id, dto);
  }
);

const createUserFx = domain.createEffect((dto: json.users.CreateUserDto) => {
  return json.users.repo.create(dto);
});

const deleteUserFx = domain.createEffect((id: number) => {
  return json.users.repo.remove(id);
});

forward({
  from: [updateUserFx, createUserFx, deleteUserFx],
  to: getUsersFx,
});

export const stores = { $users, $selectedUser };
export const actions = {
  getUsersFx,
  getUserFx,
  updateUserFx,
  createUserFx,
  deleteUserFx,
  selectUserId,
  resetUsers,
  resetSelectedUser,
};
