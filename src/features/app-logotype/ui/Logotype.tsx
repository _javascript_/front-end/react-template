import React from "react";
import { Link } from "react-router-dom";
import { PATH_HOME } from "shared/config/paths";

import "./Logotype.css";

type LogotypeProps = {};

export const Logotype = (props: LogotypeProps) => {
  return (
    <Link className="logotype" to={PATH_HOME}>
      ReactTemplate
    </Link>
  );
};
