import React from "react";
import ReactDOM from "react-dom";

import "./Widget.css";

export type DeveloperWidgetProps = {
  children: React.ReactNode;
};

export const DeveloperWidget: React.FC<DeveloperWidgetProps> = ({
  children,
}) => {
  return ReactDOM.createPortal(
    <article className="developer__widget">{children}</article>,
    document.body
  );
};
