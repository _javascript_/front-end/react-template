import React, { useContext, useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Drawer,
} from "shared/ui";

import { appContextConfig, appContextUtils } from "features/app-context";

import "./Developer.css";

export type DeveloperProps = {};

export const DeveloperContent = (props: DeveloperProps) => {
  const ctx = useContext(appContextConfig.ApplicationContext);
  const [open, setOpen] = useState<boolean>(false);
  let obj = { ...ctx } as any;
  delete obj.actions;
  return (
    <>
      <Button className="developer" onClick={() => setOpen(true)}>
        Dev
      </Button>
      <Drawer open={open} onClose={() => setOpen(false)}>
        <Card className="developer__card">
          <CardHeader>Developoer content</CardHeader>
          <CardBody>{renderObject(obj)}</CardBody>
          <CardFooter>
            <Button onClick={() => appContextUtils.addRole(ctx, "USER")}>
              Add Role USER
            </Button>
            <Button onClick={() => appContextUtils.addRole(ctx, "ADMIN")}>
              Add Role ADMIN
            </Button>
          </CardFooter>
        </Card>
      </Drawer>
    </>
  );
};

function renderObject(obj?: object) {
  if (!obj) return null;
  return (
    <ul>
      {Object.entries(obj).map(([key, value]) => {
        if (typeof value === "object") {
          if (Array.isArray(value)) {
            value = value.join(", ");
          } else value = renderObject(value);
        }
        if (typeof value === "boolean") {
          value = value ? "true" : "false";
        }
        return (
          <li key={key}>
            {key}: {value ?? "null"}
          </li>
        );
      })}
    </ul>
  );
}
