/* global clearTimeout */
import { useState, useEffect, useMemo } from "react";
import { useNavigate } from "react-router";

import { GET_PARAMS } from "../config";
import { useGetParameter } from "../utils";

let timeout: NodeJS.Timeout;

function parseStringifiedValue(value: string | null) {
  return value ? value.split(",") : [];
}

export const useGetPopupsState = () => {
  const navigate = useNavigate();
  const rawPopups = useGetParameter(GET_PARAMS.popup);
  const [mountedPopups, setMountedPopups] = useState(
    parseStringifiedValue(rawPopups)
  );

  const goBack = () => navigate(-1);

  useEffect(() => {
    if (rawPopups) {
      timeout && clearTimeout(timeout);
      setMountedPopups(rawPopups.split(","));
    } else {
      timeout = setTimeout(() => {
        setMountedPopups([]);
      }, 300);
    }
  }, [rawPopups]);

  useEffect(() => {
    return () => {
      timeout && clearTimeout(timeout);
    };
  }, []);

  const popups = useMemo(() => parseStringifiedValue(rawPopups), [rawPopups]);

  return {
    mountedPopups,
    popups,
    goBack,
  };
};
