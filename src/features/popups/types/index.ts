export type InjectedComponentProps = {
  isOpened: boolean;
  goBack: () => void;
};

export type MappedPopups = {
  [name: string]: (props: InjectedComponentProps) => JSX.Element;
};

export type GetParameterPopupsProps = {
  components: MappedPopups;
};
