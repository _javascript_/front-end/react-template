export * as PopupsUI from "./ui";
export * as popupsConfig from "./config";
export * as popupsService from "./service";
export * as popupsTypes from "./types";
export * as popupsUtils from "./utils";
