import { useGetPopupsState } from "../service";
import { GetParameterPopupsProps } from "../types";

export const GetParameterPopups = (props: GetParameterPopupsProps): any => {
  const { mountedPopups, popups, goBack } = useGetPopupsState();

  return mountedPopups.map((mountedPopup) => {
    const Component = props.components?.[mountedPopup];

    if (!Component) {
      return null;
    }

    return (
      <Component
        key={mountedPopup}
        isOpened={popups.includes(mountedPopup)}
        goBack={goBack}
      />
    );
  });
};
