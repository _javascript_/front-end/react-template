import type { dto, entities, types } from "shared/api/my-api";

export type ApplicationContextState = {
  initialization: boolean;
  finalization: boolean;
  token: entities.AccessToken | null;
  account: entities.Account;
  roles: types.RoleValue[];
};

export type ApplicationContextActions = {
  auth: ApplicationContextAuthActions;
  ctx: {
    updateStateByToken: (token: entities.AccessToken) => void;
    set: <T extends Key>(key: T, value: Value<T>) => void;
  };
};

export type ApplicationContextAuthActions = {
  signUp: (dto: dto.SignUpDto) => Promise<void>;
  signIn: (dto: dto.SignInDto) => Promise<void>;
  signOut: () => Promise<void>;
  refresh: () => Promise<void>;
};

type Key = keyof ApplicationContextState;
type Value<T extends Key> = ApplicationContextState[T];

export type ApplicationContextType = ApplicationContextState & {
  actions: ApplicationContextActions;
};
