import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";

import { myServer } from "shared/api";
import { logger } from "shared/lib";

import { ApplicationContextState } from "../types";
import { DEFAULT_STATE } from "../config";

const DEFAULT_NAVIGATE = "/";

export const useCreateApplicationContext = () => {
  type Key = keyof ApplicationContextState;
  type Value<T extends Key> = ApplicationContextState[T];

  const navigate = useNavigate();
  const location = useLocation();

  const [state, setState] = useState<ApplicationContextState>(DEFAULT_STATE);

  function set<T extends Key>(key: T, value: Value<T>) {
    logger.warn("Set context", { key, value });
    setState((prev) => {
      return { ...prev, [key]: value };
    });
  }

  useEffect(() => {
    logger.log("Context State", state);
  }, [state]);

  async function signIn(dto: myServer.auth.SignInDto) {
    const token = await myServer.auth.repo.signIn(dto);
    updateStateByToken(token);
    const to = (location as any)?.state?.prevPath ?? DEFAULT_NAVIGATE;
    navigate(to);
  }
  async function signUp(dto: myServer.auth.SignUpDto) {
    const token = await myServer.auth.repo.signUp(dto);
    updateStateByToken(token);
  }
  async function signOut() {
    await myServer.auth.repo.signOut();
    reset();
  }
  async function refresh() {
    const token = await myServer.auth.repo.refresh();
    updateStateByToken(token);
  }

  function updateStateByToken(token: myServer.auth.AccessToken) {
    myServer.auth.AccessToken.save(token);
    const account = token.decode();
    set("token", token);
    set("account", account);
    set("roles", account.roles);
  }

  function reset() {
    setState({ ...DEFAULT_STATE, initialization: false });
  }

  return {
    ...state,
    actions: {
      auth: {
        signIn,
        signUp,
        signOut,
        refresh,
      },
      ctx: {
        updateStateByToken,
        set,
      },
    },
  };
};
