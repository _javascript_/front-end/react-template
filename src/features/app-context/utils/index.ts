import { common } from "shared/api";
import { ApplicationContextType } from "../types";

export const isAppReady = (ctx: ApplicationContextType) => {
  return !ctx.initialization;
};

export const isNotAppReady = (ctx: ApplicationContextType) => {
  return ctx.initialization;
};

export const isGuest = (ctx: ApplicationContextType) => {
  return ctx.roles.includes("GUEST");
};

export const addRole = (
  ctx: ApplicationContextType,
  role: common.RoleValue
) => {
  return ctx.actions.ctx.set("roles", [...ctx.roles, role]);
};

export const removeRole = (
  ctx: ApplicationContextType,
  role: common.RoleValue
) => {
  return ctx.actions.ctx.set("roles", [...ctx.roles.filter((r) => r !== role)]);
};
