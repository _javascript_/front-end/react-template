import React from "react";
import { ApplicationContextType } from "../types";

import { myServer } from "shared/api";

const DEFAULT_ACCOUNT = myServer.accounts.Account.default();

export const DEFAULT_STATE = {
  finalization: false,
  initialization: true,
  account: DEFAULT_ACCOUNT,
  roles: DEFAULT_ACCOUNT.roles,
  token: myServer.auth.AccessToken.get(),
};

export const ApplicationContext = React.createContext<ApplicationContextType>(
  {} as ApplicationContextType
);
