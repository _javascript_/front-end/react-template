import { RouteObject, Navigate, useLocation, useNavigate } from "react-router";

import { RouteConfig } from "shared/lib/router";
import { getPageNameByPath, PATH_ERROR_403 } from "shared/config/paths";
import { LayoutVariants } from "shared/ui";
import { PAGE_NAMES } from "shared/config/pages";
import { PATHS } from "shared/config/paths";

export const useGuardRoutes = (
  configs: RouteConfig[],
  currentAccess: string[] = []
): RouteObject[] => {
  return configs.map(({ metadata, route }) => {
    const { access } = metadata;

    if (!access || !access.allowed || !access.allowed.length) return route;

    let element = route.element;

    if (!haveAccess(currentAccess, access.allowed)) {
      let redirect = PATH_ERROR_403;
      if (access.redirect) {
        if (typeof access.redirect === "string") {
          redirect = access.redirect;
        } else {
          const dominantRole = currentAccess[currentAccess.length - 1];
          redirect = access.redirect[dominantRole] ?? PATH_ERROR_403;
        }
      }
      element = (
        <Navigate
          to={`/${redirect === "/" ? "" : redirect}`}
          replace
          state={{ prevPath: route.path }}
        />
      );
    }

    return { ...route, element };
  });
};

function haveAccess(viewerRoles: string[], targetRoles: string[]): boolean {
  return viewerRoles.some((role) =>
    targetRoles.some((tRole) => role === tRole)
  );
}

export const useGetCurrentPage = () => {
  const { pathname } = useLocation();
  return getPageNameByPath(pathname);
};

export const useCurrentLayoutVariant = (configs: RouteConfig[]) => {
  const currentPage = useGetCurrentPage();

  const conf = configs.find(
    ({ metadata: { pageName } }) => pageName === currentPage
  );

  if (!conf) return LayoutVariants.HeaderFooter;

  return conf.metadata.layout;
};

export const isAdmin = (currentPage: string): boolean => {
  return currentPage === PAGE_NAMES.ADMIN;
};

export const isAuth = (currentPage: string): boolean => {
  return (
    currentPage === PAGE_NAMES.SIGN_UP || currentPage === PAGE_NAMES.SIGN_IN
  );
};

export const useAppNavigation = () => {
  const navigate = useNavigate();

  return {
    [PAGE_NAMES.UNPROTECTED]: () => navigate(`/`),
    [PAGE_NAMES.ADMIN]: () => navigate(`/${PATHS[PAGE_NAMES.ADMIN]()}`),
    [PAGE_NAMES.SIGN_UP]: () => navigate(`/${PATHS[PAGE_NAMES.SIGN_UP]()}`),
  };
};
