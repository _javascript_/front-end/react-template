import React from "react";
import {
  Link as BrowserLink,
  LinkProps as BrowserLinkProps,
  useMatch,
} from "react-router-dom";
import { clsx } from "shared/utils";
import { useTranslation } from "react-i18next";

import { PATHS } from "shared/config/paths";
import { PAGE_NAMES } from "shared/config/pages";

import { LinksProps } from "../types";

import "./Links.css";

export const Links = ({ variant = "horizontally" }: LinksProps) => {
  const { t } = useTranslation("pages");
  const links = Object.values(PATHS)
    .filter((_, index) => index < 2)
    .map((fn) => fn("404"));
  return (
    <nav
      className={clsx("navigation", [
        { navigation_variant_vertically: variant === "vertically" },
      ])}
    >
      {links.map((path) => {
        return (
          <Link key={path} to={path}>
            {t(path === "/" ? PAGE_NAMES.UNPROTECTED : path.split("/")[0])}
          </Link>
        );
      })}
    </nav>
  );
};

export const Link = (props: BrowserLinkProps) => {
  const isSelected = useMatch({
    path: props.to as string,
    end: true,
  });
  return (
    <BrowserLink
      className={clsx("navigation__item", {
        navigation__item_selected: isSelected,
      })}
      {...props}
    />
  );
};
