import { clsx } from "shared/utils";
import React from "react";

import "./Form.css";

type Field = {
  label: string;
  default?: string;
  placeholder?: string;
};

type AuthFormProps = {
  className?: string;
  title: string;
  fields: Field[];
  submit: {
    label: string;
    action: (args: any) => void;
  };
};

export const AuthForm = (props: AuthFormProps) => {
  const { fields, submit, title } = props;

  const [state, setState] = React.useState(
    fields.reduce<{ [key: string]: string }>((acc, item) => {
      acc[item.label] = item.default ?? "";
      return acc;
    }, {})
  );

  function onChange({
    target: { name, value },
  }: React.ChangeEvent<HTMLInputElement>) {
    setState((prev) => {
      return { ...prev, [name]: value };
    });
  }

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        submit.action(state);
      }}
      className={clsx("form", props.className)}
    >
      <h3>{title}</h3>

      {fields.map(({ label, placeholder }) => {
        return (
          <label key={label}>
            <p>{label}</p>
            <input
              placeholder={placeholder}
              onChange={onChange}
              name={label}
              value={state[label] ?? ""}
            />
          </label>
        );
      })}

      <br />
      <br />
      <button type="submit">{submit.label}</button>
    </form>
  );
};
