import React from "react";
import { Button } from "shared/ui";

type SignOutFormProps = {
  signOut: () => void;
};
export const SignOutButton = ({ signOut }: SignOutFormProps) => {
  return (
    <Button className="sign-out__button" onClick={() => signOut()}>
      Sign out
    </Button>
  );
};
